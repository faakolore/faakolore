<?php

use App\Models\Vehicle\BodyType;
use App\Models\Vehicle\TypeBySize;
use App\Models\Vehicle\VehicleClassification;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_types', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(VehicleClassification::class)->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignIdFor(BodyType::class)->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreignIdFor(TypeBySize::class)->constrained()->cascadeOnDelete()->cascadeOnUpdate();
            $table->string('name');
            $table->text('image');
            $table->string('bucket_size');//volumetric space (m) length X width X height
            $table->unsignedDouble('payload_capacity')->default(0);// (kg)
            $table->unsignedDouble('GVW')->default(0);// (kg)
            $table->unsignedDouble('towing_capacity',8,5)->default(0);
            $table->unsignedDouble('max_mileage')->default(0);//maximum endurance mileage (m)
            $table->timestamps();

//            $table->unique([VehicleClassification::class,BodyType::class,TypeBySize::class]);
        });
        $this->insertData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_types');
    }

    /**
     * @return bool
     */
    public function insertData(): bool
    {
        $data = [
            [
                'name' => 'motorbike',
                'vehicle_classification_id' => 1,
                'body_type_id' => 1,
                'type_by_size_id' => 1,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage'  => 0,
                'GVW' => 0,
                'created_at'=> now(),
                'updated_at' => now()
                ],

            [
                'name' => 'low box tricycle// motor king',
                'vehicle_classification_id' => 1,
                'body_type_id' => 2,
                'type_by_size_id' => 1,
                'image' => '',
                'bucket_size' => '1.7x1.2',
                'payload_capacity' => 500,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'box tricycle',
                'vehicle_classification_id' => 1,
                'body_type_id' => 3,
                'type_by_size_id' => 1,
                'image' => '',
                'bucket_size' => '2x1.3x1.25',
                'payload_capacity' => 650,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'grain box tricycle',
                'vehicle_classification_id' => 1,
                'body_type_id' => 4,
                'type_by_size_id' => 2,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'lt4 van',
                'vehicle_classification_id' => 2,
                'body_type_id' => 1,
                'type_by_size_id' => 3,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],
            [
                'name' => 'lt4 low box truck',
                'vehicle_classification_id' => 2,
                'body_type_id' => 2,
                'type_by_size_id' => 2,
                'image' => '',
                'bucket_size' => '2.3×1.56×0.36',
                'payload_capacity' => 1425,
                'max_mileage' => 700,
                'GVW' => 2505
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'lt4 box truck',
                'vehicle_classification_id' => 2,
                'body_type_id' => 3,
                'type_by_size_id' => 3,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'lt4 grain truck',
                'vehicle_classification_id' => 2,
                'body_type_id' => 4,
                'type_by_size_id' => 3,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'lt4 reefer truck',
                'vehicle_classification_id' => 2,
                'body_type_id' => 5,
                'type_by_size_id' => 3,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'lt4 stake side truck',
                'vehicle_classification_id' => 2,
                'body_type_id' => 8,
                'type_by_size_id' => 3,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su2 van',
                'vehicle_classification_id' => 3,
                'body_type_id' => 1,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su2 low box truck',
                'vehicle_classification_id' => 3,
                'body_type_id' => 2,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su2 box truck',
                'vehicle_classification_id' => 3,
                'body_type_id' => 3,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su2 grain box truck',
                'vehicle_classification_id' => 3,
                'body_type_id' => 4,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su2 reefer truck',
                'vehicle_classification_id' => 3,
                'body_type_id' => 5,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su2 flat bed truck',
                'vehicle_classification_id' => 3,
                'body_type_id' => 6,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su2 stake side bed truck',
                'vehicle_classification_id' => 3,
                'body_type_id' => 8,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su3 low box truck',
                'vehicle_classification_id' => 4,
                'body_type_id' => 2,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su3 box truck',
                'vehicle_classification_id' => 4,
                'body_type_id' => 3,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su3 grain box truck',
                'vehicle_classification_id' => 4,
                'body_type_id' => 4,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su3 reefer truck',
                'vehicle_classification_id' => 4,
                'body_type_id' => 5,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su3 flatbed truck',
                'vehicle_classification_id' => 4,
                'body_type_id' => 6,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su3 semi-trailer',
                'vehicle_classification_id' => 4,
                'body_type_id' => 7,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su3 stake side bed',
                'vehicle_classification_id' => 4,
                'body_type_id' => 8,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su4+ low box truck',
                'vehicle_classification_id' => 5,
                'body_type_id' => 2,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su4+ box truck',
                'vehicle_classification_id' => 5,
                'body_type_id' => 3,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su4+ grain box truck',
                'vehicle_classification_id' => 5,
                'body_type_id' => 4,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su4+ reefer truck',
                'vehicle_classification_id' => 5,
                'body_type_id' => 5,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su4+ flatbed truck',
                'vehicle_classification_id' => 5,
                'body_type_id' => 6,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su4+ semi-trailer',
                'vehicle_classification_id' => 5,
                'body_type_id' => 7,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],

            [
                'name' => 'su4+ stake side bed truck',
                'vehicle_classification_id' => 5,
                'body_type_id' => 8,
                'type_by_size_id' => 4,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs3 low box truck',
                'vehicle_classification_id' => 6,
                'body_type_id' => 2,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs3 box truck',
                'vehicle_classification_id' => 6,
                'body_type_id' => 3,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs3 grain box truck',
                'vehicle_classification_id' => 6,
                'body_type_id' => 4,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs3 reefer truck',
                'vehicle_classification_id' => 6,
                'body_type_id' => 5,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs3 flatbed truck',
                'vehicle_classification_id' => 6,
                'body_type_id' => 6,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs3 semi trailer',
                'vehicle_classification_id' => 6,
                'body_type_id' => 7,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs3 stake side bed truck',
                'vehicle_classification_id' => 6,
                'body_type_id' => 8,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs4 low box truck',
                'vehicle_classification_id' => 7,
                'body_type_id' => 2,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs4 box truck',
                'vehicle_classification_id' => 7,
                'body_type_id' => 3,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs4 grain box truck',
                'vehicle_classification_id' => 7,
                'body_type_id' => 4,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs4 reefer truck',
                'vehicle_classification_id' => 7,
                'body_type_id' => 5,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs4 flatbed truck',
                'vehicle_classification_id' => 7,
                'body_type_id' => 6,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs4 semi trailer',
                'vehicle_classification_id' => 7,
                'body_type_id' => 7,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs4 stake side bed truck',
                'vehicle_classification_id' => 7,
                'body_type_id' => 8,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs5t low box truck',
                'vehicle_classification_id' => 8,
                'body_type_id' => 2,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs5t box truck',
                'vehicle_classification_id' => 8,
                'body_type_id' => 3,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs5t grain box truck',
                'vehicle_classification_id' => 8,
                'body_type_id' => 4,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs5t reefer truck',
                'vehicle_classification_id' => 8,
                'body_type_id' => 5,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs5t flatbed truck',
                'vehicle_classification_id' => 8,
                'body_type_id' => 6,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs5t semi-tractor',
                'vehicle_classification_id' => 8,
                'body_type_id' => 7,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs5t stake side bed truck',
                'vehicle_classification_id' => 8,
                'body_type_id' => 8,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs5s low box truck',
                'vehicle_classification_id' => 9,
                'body_type_id' => 2,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs5s box truck',
                'vehicle_classification_id' => 9,
                'body_type_id' => 3,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs5s grain box truck',
                'vehicle_classification_id' => 9,
                'body_type_id' => 4,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs5s reefer truck',
                'vehicle_classification_id' => 9,
                'body_type_id' => 5,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs5s flatbed truck',
                'vehicle_classification_id' => 9,
                'body_type_id' => 6,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs5s semi trailer',
                'vehicle_classification_id' => 9,
                'body_type_id' => 7,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs5s stake side bed truck',
                'vehicle_classification_id' => 9,
                'body_type_id' => 8,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs6+ low bed truck',
                'vehicle_classification_id' => 10,
                'body_type_id' => 2,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs6+ box truck',
                'vehicle_classification_id' => 10,
                'body_type_id' => 3,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs6+ grain box truck',
                'vehicle_classification_id' => 10,
                'body_type_id' => 4,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs6+ reefer truck',
                'vehicle_classification_id' => 10,
                'body_type_id' => 5,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs6+ flatbed truck',
                'vehicle_classification_id' => 10,
                'body_type_id' => 6,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs6+ semi-tractor',
                'vehicle_classification_id' => 10,
                'body_type_id' => 7,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs6+ stake-side bed truck',
                'vehicle_classification_id' => 10,
                'body_type_id' => 8,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs7+ low bed truck',
                'vehicle_classification_id' => 11,
                'body_type_id' => 2,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs7+ box truck',
                'vehicle_classification_id' => 11,
                'body_type_id' => 3,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs7+ grain box truck',
                'vehicle_classification_id' => 11,
                'body_type_id' => 4,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs7+ reefer truck',
                'vehicle_classification_id' => 11,
                'body_type_id' => 5,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs7+ flatbed truck',
                'vehicle_classification_id' => 11,
                'body_type_id' => 6,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs7+ semi-tractor',
                'vehicle_classification_id' => 11,
                'body_type_id' => 7,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'cs7+ stake side bed truck',
                'vehicle_classification_id' => 11,
                'body_type_id' => 8,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct34 low bed truck',
                'vehicle_classification_id' => 12,
                'body_type_id' => 2,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct34 box truck',
                'vehicle_classification_id' => 12,
                'body_type_id' => 3,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct34 grain box truck',
                'vehicle_classification_id' => 12,
                'body_type_id' => 4,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct34 reefer truck',
                'vehicle_classification_id' => 12,
                'body_type_id' => 5,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct34 flatbed truck',
                'vehicle_classification_id' => 12,
                'body_type_id' => 6,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct34 semi-tractor',
                'vehicle_classification_id' => 12,
                'body_type_id' => 7,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct34 stake side bed truck',
                'vehicle_classification_id' => 12,
                'body_type_id' => 8,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct5 low box truck',
                'vehicle_classification_id' => 13,
                'body_type_id' => 2,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct5 box truck',
                'vehicle_classification_id' => 13,
                'body_type_id' => 3,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct5 grain box truck',
                'vehicle_classification_id' => 13,
                'body_type_id' => 4,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct5 reefer truck',
                'vehicle_classification_id' => 13,
                'body_type_id' => 5,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct5 flatbed truck',
                'vehicle_classification_id' => 13,
                'body_type_id' => 6,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct5 semi-tractor',
                'vehicle_classification_id' => 13,
                'body_type_id' => 7,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct5 stake side bed truck',
                'vehicle_classification_id' => 13,
                'body_type_id' => 8,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct6+ low box truck',
                'vehicle_classification_id' => 14,
                'body_type_id' => 2,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct6+ box truck',
                'vehicle_classification_id' => 14,
                'body_type_id' => 3,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct6+ grain box truck',
                'vehicle_classification_id' => 14,
                'body_type_id' => 4,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct6+ reefer truck',
                'vehicle_classification_id' => 14,
                'body_type_id' => 5,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct6+ flatbed truck',
                'vehicle_classification_id' => 14,
                'body_type_id' => 6,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct6+ semi-tractor',
                'vehicle_classification_id' => 14,
                'body_type_id' => 7,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ct6+ stake-side bed truck',
                'vehicle_classification_id' => 14,
                'body_type_id' => 8,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds5 low box truck',
                'vehicle_classification_id' => 15,
                'body_type_id' => 2,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds5 box truck',
                'vehicle_classification_id' => 15,
                'body_type_id' => 3,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds5 grain box truck',
                'vehicle_classification_id' => 15,
                'body_type_id' => 4,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds5 reefer truck',
                'vehicle_classification_id' => 15,
                'body_type_id' => 5,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds5 flatbed truck',
                'vehicle_classification_id' => 15,
                'body_type_id' => 6,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds5 semi-tractor',
                'vehicle_classification_id' => 15,
                'body_type_id' => 7,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds5 stake-side bed truck',
                'vehicle_classification_id' => 15,
                'body_type_id' => 8,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds6 low box truck',
                'vehicle_classification_id' => 16,
                'body_type_id' => 2,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds6 box truck',
                'vehicle_classification_id' => 16,
                'body_type_id' => 3,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds6 grain box truck',
                'vehicle_classification_id' => 16,
                'body_type_id' => 4,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds6 reefer truck',
                'vehicle_classification_id' => 16,
                'body_type_id' => 5,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds6 flatbed truck',
                'vehicle_classification_id' => 16,
                'body_type_id' => 6,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds6 semi-tractor',
                'vehicle_classification_id' => 16,
                'body_type_id' => 7,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds6 stake-side bed truck',
                'vehicle_classification_id' => 16,
                'body_type_id' => 8,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds7 low box truck',
                'vehicle_classification_id' => 17,
                'body_type_id' => 2,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds7 box truck',
                'vehicle_classification_id' => 17,
                'body_type_id' => 3,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds7 grain box truck',
                'vehicle_classification_id' => 17,
                'body_type_id' => 4,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds7 reefer truck',
                'vehicle_classification_id' => 17,
                'body_type_id' => 5,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds7 flatbed truck',
                'vehicle_classification_id' => 17,
                'body_type_id' => 6,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds7 semi-tractor',
                'vehicle_classification_id' => 17,
                'body_type_id' => 7,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds7 stake-side bed truck',
                'vehicle_classification_id' => 17,
                'body_type_id' => 8,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds8+ low box truck',
                'vehicle_classification_id' => 18,
                'body_type_id' => 2,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds8+ box truck',
                'vehicle_classification_id' => 18,
                'body_type_id' => 3,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds8+ grain box truck',
                'vehicle_classification_id' => 18,
                'body_type_id' => 4,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds8+ reefer truck',
                'vehicle_classification_id' => 18,
                'body_type_id' => 5,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds8+ flatbed truck',
                'vehicle_classification_id' => 18,
                'body_type_id' => 6,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds8+ semi-tractor',
                'vehicle_classification_id' => 18,
                'body_type_id' => 7,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'ds8+ stake-side bed truck',
                'vehicle_classification_id' => 18,
                'body_type_id' => 8,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'trpl low box truck',
                'vehicle_classification_id' => 19,
                'body_type_id' => 2,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'trpl box truck',
                'vehicle_classification_id' => 19,
                'body_type_id' => 3,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'trpl grain box truck',
                'vehicle_classification_id' => 19,
                'body_type_id' => 4,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'trpl reefer truck',
                'vehicle_classification_id' => 19,
                'body_type_id' => 5,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'trpl flatbed truck',
                'vehicle_classification_id' => 19,
                'body_type_id' => 6,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


            [
                'name' => 'trpl stake-side bed truck',
                'vehicle_classification_id' => 19,
                'body_type_id' => 8,
                'type_by_size_id' => 5,
                'image' => '',
                'bucket_size' => 0,
                'payload_capacity' => 0,
                'max_mileage' => 0,
                'GVW' => 0
             ,'created_at'=> now(),
             'updated_at' => now()
            ],


//            [
//                'name' => 0,
//                'vehicle_classification_id' => 0,
//                'body_type_id' => 0,
//                'type_by_size_id' => 5,
//                'image' => '',
//                'bucket_size' => 0,
//                'payload_capacity' => 0,
//                'max_mileage' => 0,
//                'GVW' => 0
//             ,'created_at'=> now(),
//             'updated_at' => now()
//            ],
        ];

        return DB::table('vehicle_types')->insert($data);
    }

}
