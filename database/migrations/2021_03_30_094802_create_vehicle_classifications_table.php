<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleClassificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_classifications', function (Blueprint $table)
        {
            $table->id();
            $table->string('class')->unique();
            $table->string('notation')->unique();
            $table->text('description');
            $table->unsignedBigInteger('GVWR')->default(0);//kg
            $table->timestamps();
        });
        $this->insertData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_classifications');
    }

    /**
     * @return bool
     */
    public function insertData(): bool
    {
        $data = [
            ['class'=>'Class 1' ,
             'notation' => 'MOTOR',
             'description'=> 'Tricycles and motorcycles',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 2' ,
             'notation' => 'LT4',
             'description'=> 'Light trucks with 2 axles and 4 tires (pickup trucks, vans, minivans, etc.)',
             'GVWR' => 3175.147,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 3' ,
             'notation' => 'SU2',
             'description'=> 'Single-unit, 2 axle, 6 tire trucks (includes SU2 pulling a utility trailer)',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 4' ,
             'notation' => 'SU3',
             'description'=> 'Single-unit, 3 axle trucks (includes SU3 pulling a utility trailer)',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 5' ,
             'notation' => 'SU4+',
             'description'=> 'Single-unit trucks with 4 or more axles (includes SU4+ pulling a utility trailer)',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 6' ,
             'notation' => 'CS3',
             'description'=> 'Tractor-semitrailer combinations with 3 axles',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 7' ,
             'notation' => 'CS4',
             'description'=> 'Tractor-semitrailer combinations with 4 axles',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 8' ,
             'notation' => 'CS5T',
             'description'=> 'Tractor-semitrailer combinations with 5 axles, two rear tandem axles',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 9' ,
             'notation' => 'CS5S',
             'description'=> 'Tractor-semitrailer combinations with 5 axles, two split (.8) rear axles',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 10' ,
             'notation' => 'CS6+',
             'description'=> 'Tractor-semitrailer combinations with 6 or more axles',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 11' ,
             'notation' => 'CS7+',
             'description'=> 'Tractor-semitrailer combinations with 7 or more axles',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 12' ,
             'notation' => 'CT34',
             'description'=> 'Truck-trailers combinations with 3 or 4 axles',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 13' ,
             'notation' => 'CT5',
             'description'=> 'Truck-trailers combinations with 5 axles',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 14' ,
             'notation' => 'CT6+',
             'description'=> 'Truck-trailers combinations with 6 or more axles',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 15' ,
             'notation' => 'DS5',
             'description'=> 'Tractor-double semitrailer combinations with 5 axles',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 16' ,
             'notation' => 'DS6',
             'description'=> 'Tractor-double semitrailer combinations with 6 axles',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 17' ,
             'notation' => 'DS7',
             'description'=> 'Tractor-double semitrailer combinations with 7 axles',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 18' ,
             'notation' => 'DS8+',
             'description'=> 'Tractor-double semitrailer combinations with 8 or more axles',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

            ['class'=>'Class 19' ,
             'notation' => 'TRPL',
             'description'=> 'Tractor-triple semitrailer or truck-double semitrailer combinations',
             'GVWR' => 0,
             'created_at'=> now(),
             'updated_at' => now()],

//            ['class'=>'Class ' ,
//             'notation' => '',
//             'description'=> '',
//             'GVWR' => '',
//             'created_at'=> now(),
//             'updated_at' => now()],
        ];
        return \Illuminate\Support\Facades\DB::table('vehicle_classifications')->insert($data);
    }
}
