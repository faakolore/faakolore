<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypeBySizesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_by_sizes', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->longText('description');
            $table->text('url');
            $table->timestamps();
        });
        $this->insertData();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type_by_sizes');
    }

    /**
     * @return bool
     */
    public function insertData(): bool
    {
        $data = [
            //1
            ['title'=> 'Ultra light duty',
             'description'=> 'Often produced as variations of golf cars, with internal combustion or battery electric drive, these are used typically for off-highway use on estates, golf courses, and parks. While not suitable for highway use some variations may be licensed as slow speed vehicles for operation on streets, generally as a body variation of a neighborhood electric vehicle. A few manufactures produce specialized chassis for this type of vehicle, while Zap Motors markets a version of their Xebra electric tricycle (licensable in the U.S. as a motorcycle).',
             'url' => 'https://en.wikipedia.org/wiki/Truck#Ultra_light',
             'created_at'=> now(),
             'updated_at' => now()],
            //2
            ['title'=> 'Very Light duty',
             'description'=> 'Specialized designs with substantial frames such as the Italian Piaggio shown here are based upon Japanese designs (in this case by Daihatsu) and are popular for use in "old town" sections of European cities that often have very narrow alleyways.
Regardless of name, these small trucks serve a wide range of uses. In Japan, they are regulated under the Kei car laws, which allow vehicle owners a break in taxes for buying a smaller and less-powerful vehicle (currently, the engine is limited to 660 cc displacement). These vehicles are used as on-road utility vehicles in Japan.',
             'url' => 'https://en.wikipedia.org/wiki/Truck#Very_light',
             'created_at'=> now(),
             'updated_at' => now()],
            //3
            ['title'=> 'Light duty',
             'description'=> 'Light trucks are car-sized (in the US, no more than 13,900 lb (6.3 t)) and are used by individuals and businesses alike. In the EU they may not weigh more than 3.5 t (7,700 lb) and are allowed to be driven with a driving licence for cars. Pickup trucks, called utes in Australia and New Zealand, are common in North America and some regions of Latin America, Asia, and Africa, but not so in Europe, where this size of commercial vehicle is most often made as vans.' ,
             'url' => 'https://en.wikipedia.org/wiki/Truck#Light',
             'created_at'=> now(),
             'updated_at' => now()],
            //4
            ['title'=> 'Medium duty',
             'description'=> 'Medium trucks are larger than light but smaller than heavy trucks. In the US, they are defined as weighing between 13,000 and 33,000 lb (5.9 and 15.0 t). For the UK and the EU the weight is between 3.5 to 7.5 t (7,700 to 16,500 lb). Local delivery and public service (dump trucks, garbage trucks and fire-fighting trucks) are normally around this size.',
             'url' => 'https://en.wikipedia.org/wiki/Truck#Medium',
             'created_at'=> now(),
             'updated_at' => now()],
            //5
            ['title'=> 'Heavy duty',
             'description'=> 'Heavy trucks are the largest on-road trucks, Class 8. These include vocational applications such as heavy dump trucks, concrete pump trucks, and refuse hauling, as well as ubiquitous long-haul 4x2 and 6×4 tractor units.
Road damage and wear increase very rapidly with the axle weight. The number of steering axles and the suspension type also influence the amount of the road wear. In many countries with good roads a six-axle truck may have a maximum weight of 44 t (97,000 lb) or more.',
             'url' => 'https://en.wikipedia.org/wiki/Truck#Heavy',
             'created_at'=> now(),
             'updated_at' => now()],
//            ['title'=> ,
//             'description'=> ,
//             'url' => ,
//             'created_at'=> now(),
//             'updated_at' => now()],
        ];
        return \Illuminate\Support\Facades\DB::table('type_by_sizes')->insert($data);
    }
}
