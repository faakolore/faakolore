<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateBodyTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('body_types', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->longText('description');
            $table->text('url');
            $table->timestamps();
        });
        $this->insert();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('body_types');
    }

    public function insert(): bool
    {
        $data = [
            //1
//            ['title'=> 'Tricycle',
//             'description'=> '3 wheel Cargo tricycles for heaving loading under rough roads.',
//             'url' => '',
//            'created_at'=> now(),
//             'updated_at' => now()],

            //1
            ['title'=> 'Van',
                'description'=> 'A van is a type of road vehicle used for transporting goods or people. Depending on the type of van, it can be bigger or smaller than a truck and SUV, and bigger than a common car. There is some varying in the scope of the word across the different English-speaking countries. The smallest vans, microvans, are used for transporting either goods or people in tiny quantities. Mini MPVs, compact MPVs, and MPVs are all small vans usually used for transporting people in small quantities. Larger vans with passenger seats are used for institutional purposes, such as transporting students. Larger vans with only front seats are often used for business purposes, to carry goods and equipment. Specially-equipped vans are used by television stations as mobile studios. Postal services and courier companies use large step vans to deliver packages.',
             'url' => 'https://en.wikipedia.org/wiki/Van',
//              'type_by_size_id' => ,
             'created_at'=> now(),
             'updated_at' => now()],

            //2
            ['title'=> 'Low Box',
                'description'=> 'Low Box is mini-truck with one cab, It stands out as known as, efficiency, low cost, safety, flexible and affordable. It’s very useful mini-truck to help delivery in town/city more road safety and look neatly for a beautiful road city.',
             'url' => 'https://www.japanmotors.com/foton/vehicles/mini_cargo/index.html',
//              'type_by_size_id' => ,
             'created_at'=> now(),
             'updated_at' => now()],

            //3
            ['title'=> 'Box trucks',
                'description'=> 'Box trucks ("tilts" in the UK) have walls and a roof, making an enclosed load space. The rear has doors for unloading; a side door is sometimes fitted.',
                'url' => 'https://en.wikipedia.org/wiki/Box_truck',
                'created_at'=> now(),
                'updated_at' => now()],

            //4
            ['title'=> 'Grain Box',
                'description'=> 'grain bodies are ideally suited for the Feed & Seed dealer or the farmer/contract hauler.  whether you have loose or palletized cargo our corrugated sides and forklift proven tongue and groove platform will not let you down.',
                'url' => 'https://agrilitetrailers.com/grain-box/',
//              'type_by_size_id' => ,
             'created_at'=> now(),
             'updated_at' => now()],

            //5
            ['title'=> 'Refrigerator trucks',
             'description'=> 'Refrigerator trucks have insulated panels as walls and a roof and floor, used for transporting fresh and frozen cargo such as ice cream, food, vegetables, and prescription drugs. They are mostly equipped with double-wing rear doors, but a side door is sometimes fitted.',
             'url' => 'https://en.wikipedia.org/wiki/Refrigerator_truck',
             'created_at'=> now(),
             'updated_at' => now()],

            //6
            ['title'=> 'Flatbed trucks',
             'description'=> 'Flatbed trucks have an entirely flat, level platform body. This allows for quick and easy loading but has no protection for the load. Hanging or removable sides are sometimes fitted.',
             'url' => 'https://en.wikipedia.org/wiki/Flatbed_trucks',
            'created_at'=> now(),
             'updated_at' => now()],

            //7
            ['title'=> 'Semi-tractors',
             'description'=> 'Semi-tractors ("artics" in the UK) have a fifth wheel for towing a semi-trailer instead of a body.',
             'url' => 'https://en.wikipedia.org/wiki/Tractor_unit',
            'created_at'=> now(),
             'updated_at' => now()],

            //8
             ['title'=> 'Stake Side bed',
             'description'=> 'stake body truck is essentially a flatbed truck with built-in sockets surrounding the edge of the bed. This enables the user to install upright stakes to create a “fence” around the load. The fencing helps to hold loads of loose materials in place during transit and can also provide additional support for taller objects.',
             'url' => 'https://www.readingbody.com/how-to-spec-the-right-stake-body/',
             'created_at'=> now(),
             'updated_at' => now()],

//            ['title'=> ,
//             'description'=> ,
//             'url' => ,
//              'type_by_size_id' => ,
//             'created_at'=> now(),
//             'updated_at' => now()],
        ];

        return DB::table('body_types')->insert($data);
    }
}
