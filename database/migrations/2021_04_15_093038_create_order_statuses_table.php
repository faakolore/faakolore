<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateOrderStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('status');
            $table->text('description');
            $table->timestamps();
        });
        $this->data();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_statuses');
    }

    /**
     * @return bool
     */
    public function data(): bool
    {
            $data = [
                ['status'=> 'processing',
             'description'=> 'The Request is matching to the most efficient available driver.',
             'created_at'=> now(),
             'updated_at' => now()],
           ['status'=> 'no_drivers_available',
             'description'=> 'The Request was unfulfilled because no drivers were available.',
             'created_at'=> now(),
             'updated_at' => now()],
           ['status'=> 'accepted',
             'description'=> 'The Request has been accepted by a driver and is “en route” to the start location (i.e. start_latitude and start_longitude). This state can occur multiple times in case of a driver re-assignment.',
             'created_at'=> now(),
             'updated_at' => now()],
           ['status'=> 'arriving',
             'description'=> 'The driver has arrived or will be shortly.',
             'created_at'=> now(),
             'updated_at' => now()],
           ['status'=> 'in_progress',
             'description'=> 'The Request is “en route” from the start location to the end location.',
             'created_at'=> now(),
             'updated_at' => now()],
           ['status'=> 'driver_cancelled',
             'description'=> 'The Request has been cancelled by the driver.',
             'created_at'=> now(),
             'updated_at' => now()],
           ['status'=> 'shipper_cancelled',
             'description'=> 'The Request cancelled by shipper.',
             'created_at'=> now(),
             'updated_at' => now()],

           ['status'=> 'in_transit',
             'description'=> 'The produce has switch vehicles',
             'created_at'=> now(),
             'updated_at' => now()],
//            ['status'=> ,
//             'description'=> ,
//             'created_at'=> now(),
//             'updated_at' => now()],
//            ['status'=> ,
//             'description'=> ,
//             'created_at'=> now(),
//             'updated_at' => now()],

            ['status'=> 'completed',
                'description'=> 'Request has been completed by the driver.',
                'created_at'=> now(),
                'updated_at' => now()],
            ];
        return DB::table('order_statuses')->insert($data);
    }
}
