<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_requests', function (Blueprint $table) {
            $table->uuid('id')->primary()->index();
            $table->morphs('userable');
//            $table->longText('pickup_location');
//            $table->longText('dropoff_location');
            $table->unsignedBigInteger('quantity');
            $table->text('load_type');
            $table->string('platform');
            $table->foreignId('carrier_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_requests');
    }
}
