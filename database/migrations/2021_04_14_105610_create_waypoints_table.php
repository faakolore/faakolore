<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWaypointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waypoints', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('type');
            $table->foreignUuid('order_request_id')->constrained()->cascadeOnDelete();
            $table->longText('address');
            $table->string('alias')->nullable();
            $table->string('name')->nullable();
            $table->longText('full_address')->nullable();
            $table->point('coordinates')->spatialIndex();
            $table->polygon('way_stops')->nullable();
            $table->longText('message')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waypoints');
    }
}
