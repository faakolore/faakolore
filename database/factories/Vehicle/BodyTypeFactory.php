<?php

namespace Database\Factories\Vehicle;

use App\Models\Vehicle\BodyType;
use Illuminate\Database\Eloquent\Factories\Factory;

class BodyTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BodyType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
