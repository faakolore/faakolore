<?php

namespace Database\Factories\Vehicle;

use App\Models\Vehicle\TypeBySize;
use Illuminate\Database\Eloquent\Factories\Factory;

class TypeBySizeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TypeBySize::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
