<?php

namespace Database\Factories\Vehicle;

use App\Models\Vehicle\RoadWorthiness;
use Illuminate\Database\Eloquent\Factories\Factory;

class RoadWorthinessFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RoadWorthiness::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
