<?php

namespace Database\Factories;

use App\Models\OperatingLane;
use Illuminate\Database\Eloquent\Factories\Factory;

class OperatingLaneFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OperatingLane::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
