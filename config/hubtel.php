<?php
/**
 * This file holds the configuration keys.
 */

return [
    'account' => [
        'key' => env('HUBTEL_CLIENT_ID'),
        'secret' => env('HUBTEL_CLIENT_SECRET'),
    ],

];
