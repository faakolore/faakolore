<?php

return [

    'OPERATING_LANE' => [
        'REGIONAL' => 0,
        'NATIONWIDE' => 1,
        'CONTINENTAL' => 2
    ],
    'WAYPOINT' => [
        'PICKUP' => 1,
        'DROPOFF' => 0,
    ],

    'LOAD_TYPE' => [
        'FRUITS_AND_VEGETABLES' => 0,
        'TUBERS' => 1,
        'GRAINS' => 2,
    ],
    'PLATFORM' => [
        'USSD'      => 0,
        'WEB'       => 1,
        'ANDROID'   => 2,
        'IOS'       => 3
    ]
];
