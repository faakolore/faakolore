<p align="center"><a href="https://laravel.com" target="_blank"><img src="storage/app/public/logo/Faakolore Logo-372x140.jpg" width="400"></a></p>

## About Faakolore


## Learning Faakolore codex

Faakolore has the most extensive and thorough [documentation]() and video tutorial library , making it a breeze to get started with the project.

If you don't feel like reading, [Faakocasts]() can help.

### Premium Partners

- **Kosmos Innovation Center**


## Code of Conduct

In order to ensure that the Faakolore community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within the application, please send an e-mail to your team lead. All security vulnerabilities will be promptly addressed.

## License

This faakolore is closed-sourced software licensed under the ...).
