<?php

use App\Actions\Fortify\EmailVerificationNotificationController;
use App\Actions\Fortify\EmailVerificationPromptController;
use App\Actions\Fortify\PhoneVerificationController;
use App\Actions\Fortify\VerifyEmailController;
use App\Http\Controllers\OrderRequestController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
require __DIR__ . '/partner.php';
require __DIR__.'/carrier.php';

Route::get('/', function () {
    return Inertia::render('HomePage', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
})->name('homepage');



Route::get('/verify-phone', [PhoneVerificationController::class, '__invoke'])
    ->middleware('auth:web')
    ->name('phone.verification.notice');
Route::post('/verify-phone', [PhoneVerificationController::class,'store'])
    ->middleware(['auth:web','throttle:2,5'])
    ->name('phone.verification.send');
Route::put('/verify-phone',[PhoneVerificationController::class,'verify'])
    ->name('phone.verification.verify');

Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])
    ->middleware('auth:web')
    ->name('verification.notice');

Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
    ->middleware(['auth:web', 'signed', 'throttle:6,1'])
    ->name('verification.verify');

Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
    ->middleware(['auth:web', 'throttle:6,1'])
    ->name('verification.send');

Route::middleware(['auth:sanctum', 'verified'])->get('/u/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

Route::get('/searching',[OrderRequestController::class,'index'])->name('searching');
Route::post('/searching',[OrderRequestController::class,'store']);
Route::put('/searching',[OrderRequestController::class,'update']);

