<?php

use App\Http\Controllers\Auth\Carriers\AuthenticatedSessionController;
use App\Http\Controllers\Auth\Carriers\CarrierRegistrationController;
use App\Http\Controllers\Auth\Carriers\ConfirmablePasswordController;
use App\Http\Controllers\Auth\Carriers\DashboardController;
use App\Http\Controllers\Auth\Carriers\PhoneVerificationController;
use App\Http\Controllers\Auth\Carriers\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\Carriers\EmailVerificationPromptController;
use App\Http\Controllers\Auth\Carriers\NewPasswordController;
use App\Http\Controllers\Auth\Carriers\PasswordResetLinkController;
use App\Http\Controllers\Auth\Carriers\RegisteredUserController;
use App\Http\Controllers\Auth\Carriers\VerifyEmailController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;


//Route::domain('carriers.'.request()->url())->group(function(){
Route::prefix('/c')->group(function (){
    Route::get('/dashboard',[DashboardController::class, 'index'])->name('carriers.dashboard');

//    Route::redirect('/','/dashboard');

    Route::get('/register', [RegisteredUserController::class, 'create'])
        ->middleware('guest:carriers')
        ->name('carriers.register');

    Route::post('/register', [RegisteredUserController::class, 'store'])
        ->middleware('guest:carriers');

    Route::get('/login', [AuthenticatedSessionController::class, 'create'])
        ->middleware('guest:carriers')
        ->name('carriers.login');

    Route::post('/login', [AuthenticatedSessionController::class, 'store'])
        ->middleware('guest:carriers');

    Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
        ->middleware('guest:carriers')
        ->name('carriers.password.request');

    Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
        ->middleware('guest:carriers')
        ->name('carriers.password.email');

    Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
        ->middleware('guest:carriers')
        ->name('carriers.password.reset');

    Route::post('/reset-password', [NewPasswordController::class, 'store'])
        ->middleware('guest:carriers')
        ->name('carriers.password.update');

    Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])
        ->middleware('auth:carriers')
        ->name('carriers.verification.notice');
    Route::get('/verify-phone', [PhoneVerificationController::class, '__invoke'])
            ->middleware('auth:carriers')
            ->name('carriers.phone.verification.notice');
    Route::post('/verify-phone', [PhoneVerificationController::class,'store'])
            ->middleware(['auth:carriers','throttle:2,5'])
            ->name('carriers.phone.verification.send');
    Route::put('/verify-phone',[PhoneVerificationController::class,'verify'])
            ->name('carriers.phone.verification.verify');

    Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
        ->middleware(['auth:carriers', 'signed', 'throttle:6,1'])
        ->name('carriers.verification.verify');

    Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
        ->middleware(['auth:carriers', 'throttle:6,1'])
        ->name('carriers.verification.send');

    Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->middleware('auth:carriers')
        ->name('carriers.password.confirm');

    Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
        ->middleware('auth:carriers');

    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->middleware('auth:carriers')
        ->name('carriers.logout');

    Route::get('/register/vehicle-and-license-details',[CarrierRegistrationController::class,'createVehicle'])
        ->name('carriers.register.vehicle');
    Route::post('/register/vehicle-and-license-details',[CarrierRegistrationController::class,'storeVehicle']);

    Route::get('/register/operating-lanes-details',[CarrierRegistrationController::class,'createLanes'])
        ->name('carriers.register.lanes');

    Route::post('/register/operating-lanes-details',[CarrierRegistrationController::class,'storeLanes']);

    Route::get('/register/proof-of-documents', [CarrierRegistrationController::class,'createProof'])
        ->name('carriers.register.proof');

    Route::post('/register/proof-of-documents',[CarrierRegistrationController::class,'storeProof']);

    Route::get('/register/complete',[CarrierRegistrationController::class,'createCompleted'])
        ->name('carriers.register.completed');

    Route::post('/register/complete',[CarrierRegistrationController::class,'storeCompleted']);

});
