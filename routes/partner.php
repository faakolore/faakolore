<?php

use App\Http\Controllers\Auth\Partners\AuthenticatedSessionController;
use App\Http\Controllers\Auth\Partners\ConfirmablePasswordController;
use App\Http\Controllers\Auth\Partners\DashboardController;
use App\Http\Controllers\Auth\Partners\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\Partners\EmailVerificationPromptController;
use App\Http\Controllers\Auth\Partners\NewPasswordController;
use App\Http\Controllers\Auth\Partners\PasswordResetLinkController;
use App\Http\Controllers\Auth\Partners\PhoneVerificationController;
use App\Http\Controllers\Auth\Partners\RegisteredUserController;
use App\Http\Controllers\Auth\Partners\VerifyEmailController;
use Illuminate\Support\Facades\Route;


//Route::domain('partners.'.request()->url())->group(function(){
Route::prefix('/p')->group(function (){

    Route::get('/dashboard', [DashboardController::class,'index'])
        ->middleware(['auth:partners'])->name('partner.dashboard');


    Route::get('/register', [RegisteredUserController::class, 'create'])
        ->middleware('guest:partners')
        ->name('partner.register');

    Route::post('/register', [RegisteredUserController::class, 'store'])
        ->middleware('guest:partners');

    Route::get('/login', [AuthenticatedSessionController::class, 'create'])
        ->middleware('guest:partners')
        ->name('partner.login');

    Route::post('/login', [AuthenticatedSessionController::class, 'store'])
        ->middleware('guest:partners');

    Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])
        ->middleware('guest:partners')
        ->name('partner.password.request');

    Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])
        ->middleware('guest:partners')
        ->name('partner.password.email');

    Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])
        ->middleware('guest:partners')
        ->name('partner.password.reset');

    Route::post('/reset-password', [NewPasswordController::class, 'store'])
        ->middleware('guest:partners')
        ->name('partner.password.update');

    Route::get('/verify-phone', [PhoneVerificationController::class, '__invoke'])
        ->middleware('auth:partners')
        ->name('partner.phone.verification.notice');
    Route::post('/verify-phone', [PhoneVerificationController::class,'store'])
        ->middleware(['auth:partners','throttle:2,5'])
        ->name('partner.phone.verification.send');
    Route::put('/verify-phone',[PhoneVerificationController::class,'verify'])
        ->name('partner.phone.verification.verify');

    Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])
        ->middleware('auth:partners')
        ->name('partner.verification.notice');

    Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])
        ->middleware(['auth:partners', 'signed', 'throttle:6,1'])
        ->name('partner.verification.verify');

    Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])
        ->middleware(['auth:partners', 'throttle:6,1'])
        ->name('partner.verification.send');

    Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])
        ->middleware('auth:partners')
        ->name('partner.password.confirm');

    Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])
        ->middleware('auth:partners');

    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])
        ->middleware('auth:partners')
        ->name('partner.logout');
});
