<?php


namespace App\Engine\Core;


use App\Models\OperatingLane;
use App\Models\OrderRequest;

class Core
{

    /**
     * @var OrderRequest
     */
    public OrderRequest $order;

    /**
     * Core constructor.
     * @param OrderRequest $orderRequest
     */
    public function __construct(OrderRequest $orderRequest)
    {
        $this->order = $orderRequest;
    }

    public function execute()
    {
        
    }

    public function callDrivers()
    {
        //Get all drivers current location using GPS
    }

    public function pickupRadius(): Core
    {
        OperatingLane::whereCity($this->order->waypoint->coordinates);
        return $this;
    }


}
