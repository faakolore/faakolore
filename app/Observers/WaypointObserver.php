<?php

namespace App\Observers;

use App\Models\Waypoint;

class WaypointObserver
{
    /**
     * Handle the Waypoint "created" event.
     *
     * @param Waypoint $waypoint
     * @return void
     */
    public function created(Waypoint $waypoint)
    {
        //
    }

    /**
     * Handle the Waypoint "updated" event.
     *
     * @param Waypoint $waypoint
     * @return void
     */
    public function updated(Waypoint $waypoint)
    {
        //
    }

    /**
     * Handle the Waypoint "deleted" event.
     *
     * @param Waypoint $waypoint
     * @return void
     */
    public function deleted(Waypoint $waypoint)
    {
        //
    }

    /**
     * Handle the Waypoint "restored" event.
     *
     * @param Waypoint $waypoint
     * @return void
     */
    public function restored(Waypoint $waypoint)
    {
        //
    }

    /**
     * Handle the Waypoint "force deleted" event.
     *
     * @param Waypoint $waypoint
     * @return void
     */
    public function forceDeleted(Waypoint $waypoint)
    {
        //
    }
}
