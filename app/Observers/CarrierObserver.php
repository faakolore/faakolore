<?php

namespace App\Observers;

use App\Models\Carrier;
use Illuminate\Support\Str;

class CarrierObserver
{
    /**
     * Handle the Carrier "created" event.
     *
     * @param  \App\Models\Carrier  $carrier
     * @return void
     */
    public function created(Carrier $carrier)
    {
        $carrier->hasVerifiedRegistration()->create([
            'vehicle' => false,
            'lanes' => false,
            'proof_of_documents' => false,
            'completed' => false,
            'verified' => false,
        ]);
    }

    /**
     * @param Carrier $carrier
     */
    public function creating(Carrier $carrier)
    {
        $carrier->uuid = Str::orderedUuid();
    }
    /**
     * Handle the Carrier "updated" event.
     *
     * @param  \App\Models\Carrier  $carrier
     * @return void
     */
    public function updated(Carrier $carrier)
    {
        //
    }

    /**
     * Handle the Carrier "deleted" event.
     *
     * @param  \App\Models\Carrier  $carrier
     * @return void
     */
    public function deleted(Carrier $carrier)
    {
        //
    }

    /**
     * Handle the Carrier "restored" event.
     *
     * @param  \App\Models\Carrier  $carrier
     * @return void
     */
    public function restored(Carrier $carrier)
    {
        //
    }

    /**
     * Handle the Carrier "force deleted" event.
     *
     * @param  \App\Models\Carrier  $carrier
     * @return void
     */
    public function forceDeleted(Carrier $carrier)
    {
        //
    }
}
