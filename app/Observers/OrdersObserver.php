<?php
//  This is the entry point of Faakolore's
//  vehicle ordering engine
//
namespace App\Observers;

use App\Engine\Core\Core;
use App\Models\OrderRequest;
use Illuminate\Support\Str;

class OrdersObserver
{
    public function creating(OrderRequest $orderRequest)
    {
        $orderRequest->id = Str::orderedUuid();
    }
    /**
     * Handle the OrderRequest "created" event.
     *
     * @param  \App\Models\OrderRequest  $orderRequest
     * @return void
     */
    public function created(OrderRequest $orderRequest)
    {
//        (new Core($orderRequest));
    }

    /**
     * Handle the OrderRequest "updated" event.
     *
     * @param  \App\Models\OrderRequest  $orderRequest
     * @return void
     */
    public function updated(OrderRequest $orderRequest)
    {
        //
    }

    /**
     * Handle the OrderRequest "deleted" event.
     *
     * @param  \App\Models\OrderRequest  $orderRequest
     * @return void
     */
    public function deleted(OrderRequest $orderRequest)
    {
        //
    }

    /**
     * Handle the OrderRequest "restored" event.
     *
     * @param  \App\Models\OrderRequest  $orderRequest
     * @return void
     */
    public function restored(OrderRequest $orderRequest)
    {
        //
    }

    /**
     * Handle the OrderRequest "force deleted" event.
     *
     * @param  \App\Models\OrderRequest  $orderRequest
     * @return void
     */
    public function forceDeleted(OrderRequest $orderRequest)
    {
        //
    }
}
