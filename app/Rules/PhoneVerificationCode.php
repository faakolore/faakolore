<?php

namespace App\Rules;

use App\Models\PhoneVerification;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class PhoneVerificationCode implements Rule
{

    private Request $request;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return $this->getPhone()->code == $value
        && Carbon::now()->diffInSeconds($this->getPhone()->created_at) < 900; //15 minutes
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'The verification code is invalid.';
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return PhoneVerification::latestVerification(
            $this->request->user()
                ->guard([
                $this->getGuard()
            ])->phone
        );
    }

    /**
     * The logic don't make sense, but I am maintaining it
     * @return mixed
     */
    public function getGuard()
    {
        return $this->request->user()->guard;
    }
}
