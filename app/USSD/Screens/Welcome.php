<?php


namespace App\USSD\Screens;


use Faakolore\USSD\Screen;

class Welcome extends Screen
{
    private string $ride ='Request for a truck';
    private string $driver = 'Partner with us as a Driver';
    private string $warehouse = 'Partner with us as a Partners Provider';
    /**
     * Add message to the screen
     *
     * @return string
     */

    protected function message(): string
    {
        return "Welcome To Faakolore USSD Platform, Please select an option";
    }

    /**
     * Add options to the screen
     * @return array
     */
    protected function options(): array
    {

        return [
            $this->ride,
            $this->driver,
            $this->warehouse
        ];
    }

    /**
    * Previous screen
    * return Screen $screen
    */
    public function previous(): Screen
    {
        return new Welcome($this->request);
    }

    /**
     * Execute the selected option/action
     *
     * @return string
     */
    protected function execute(): string
    {
        $this->addPayload('register', $this->value());
        if ($this->value()=== $this->driver)
        {
            return (new Drivers\Welcome($this->request))->render();
        }
        elseif ($this->value() === $this->warehouse)
        {
            return (new Partners\Welcome($this->request))->render();
        }
        else
        {
            return (new Farmers\Location($this->request))->render();
        }
    }

    protected function goesBack(): bool
    {
        return false;
    }
}
