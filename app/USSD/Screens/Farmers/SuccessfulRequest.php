<?php


namespace App\USSD\Screens\Farmers;


use Faakolore\USSD\Screen;

class SuccessfulRequest extends Screen
{
    protected function acceptsResponse(): bool
    {
        return false;
    }
    /**
     * Add message to the screen
     *
     * @return string
     */
    protected function message(): string
    {
        return "Your order has been confirm";
    }

    /**
     * Add options to the screen
     * @return array
     */
    protected function options(): array
    {
        return [];
    }

    /**
    * Previous screen
    * return Screen $screen
    */
    public function previous(): Screen
    {
        return (new \App\Screens\Welcome($this->request));
    }

    /**
     * Execute the selected option/action
     *
     * @return mixed
     */
    protected function execute()
    {
        return $this->previous()->render();
    }

}
