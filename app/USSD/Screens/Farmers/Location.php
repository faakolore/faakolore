<?php


namespace App\USSD\Screens\Farmers;


use Faakolore\USSD\Exceptions\UssdException;
use Faakolore\USSD\Exceptions\ValidationException;
use Faakolore\USSD\Screen;
use Faakolore\USSD\Http\Validates;
use Illuminate\Support\Collection;

class Location extends Screen
{

    use Validates;
    /**
     * Add message to the screen
     *
     * @return string
     */
    protected function message(): string
    {
        return "Where is the pickup location of the goods you want to transport? Eg: Street Name, Town/City, Region or GhanaPostGHS: eg AK1234567";
    }

    /**
     * Add options to the screen
     * @return array
     */
    protected function options(): array
    {
        return [];
    }

    /**
    * Previous screen
    * return Screen $screen
    */
    public function previous(): Screen
    {
        return new \App\USSD\Screens\Welcome($this->request);
    }

    /**
     * Execute the selected option/action
     *
     * @return mixed
     * @throws ValidationException|UssdException
     */
    protected function execute()
    {
        $this->validate($this->request,'senders_location');
        $location = $this->value();
        $validateAddress=validateDigitalAddress($location);
        if ($validateAddress==true){
            $ghanagps=GhanaPostGPS($location);
            if ($ghanagps['found']==false){
                throw new ValidationException($this->request,"Invalid Digital address, Try Again");
            }
            $newAddress=miniAddress($ghanagps);
            $this->addPayload('senders_location',$newAddress);
        }
        else{
            //Use google maps to validate address
            $this->addPayload('senders_location', $this->value());
        }

        return (new Destination($this->request))->render();
    }

    protected function rules() : string
    {
        return 'alpha_num';
    }
}
