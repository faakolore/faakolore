<?php

namespace App\USSD\Screens\Farmers;

use Faakolore\USSD\Exceptions\ValidationException;
use Faakolore\USSD\Screen;
use Faakolore\USSD\Http\Validates;
use Faakolore\USSD\Exceptions\UssdException;


class Destination extends Screen
{
    use Validates;
    /**
     * Add message to the screen
     *
     * @return string
     */
    protected function message(): string
    {
        return "Where are you sending the produce? Eg: Street Name, Town/City, Region or GhanaPostGHS: eg AK1234567";
    }

    /**
     * Add options to the screen
     * @return array
     */
    protected function options(): array
    {
        return [];
    }

    /**
    * Previous screen
    * return Screen $screen
    */
    public function previous(): Screen
    {
        return new Location($this->request);
    }

    /**
     * Execute the selected option/action
     *
     * @return mixed
     * @throws ValidationException|UssdException
     */
    protected function execute()
    {
        $this->validate($this->request,'destination');
        $destination = $this->value();
        $validateAddress = validateDigitalAddress($destination);
        if ($validateAddress==true){
            $ghanagps=ghanaPostGPS($destination);
            if ($ghanagps['found']==false){
                throw new ValidationException($this->request,"Invalid Digital address, Try Again");
            }
            $newAddress=miniAddress($ghanagps);
            $this->addPayload('destination',$newAddress);
        }
        else{
            //Use google maps to validate address
            $this->addPayload('destination', $this->value());
        }
        return (new LoadType($this->request))->render();
    }

    protected function rules() : string
    {
        return 'alpha_num';
    }
}
