<?php


namespace App\USSD\Screens\Farmers;


use Faakolore\USSD\Exceptions\UssdException;
use Faakolore\USSD\Screen;
use Faakolore\USSD\Http\Validates;
class Quantity extends Screen
{
    use Validates;
    /**
     * Add message to the screen
     *
     * @return string
     */
    protected function message(): string
    {
        return "Please enter the quantity of your produce";
    }

    /**
     * Add options to the screen
     * @return array
     */
    protected function options(): array
    {
        return [];
    }

    /**
    * Previous screen
    * return Screen $screen
    */
    public function previous(): Screen
    {
        return new LoadType($this->request);
    }

    /**
     * Execute the selected option/action
     *
     * @return mixed
     * @throws UssdException
     */
    protected function execute()
    {
        $this->validate($this->request,'quantity');
        $this->addPayload('quantity', $this->value());
        return (new ConfirmRequest($this->request))->render();
    }

    protected function rules():string
    {
        return 'numeric';
    }
}
