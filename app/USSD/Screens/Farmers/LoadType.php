<?php


namespace App\USSD\Screens\Farmers;


use Faakolore\USSD\Screen;

class LoadType extends Screen
{

    /**
     * Add message to the screen
     *
     * @return string
     */
    protected function message(): string
    {
        return "Choose a produce type from the list";
    }

    /**
     * Add options to the screen
     * @return array
     */
    protected function options(): array
    {
        return [
            "Grains",
            "Tubers",
            "Vegetables and Fruits"
        ];
    }

    /**
    * Previous screen
    * return Screen $screen
    */
    public function previous(): Screen
    {
        return new Destination($this->request);
    }

    /**
     * Execute the selected option/action
     *
     * @return mixed
     */
    protected function execute()
    {
       $this->addPayload('load_type', $this->value());
       return  (new Quantity($this->request))->render();
    }
}
