<?php


namespace App\USSD\Screens\Farmers;


use Faakolore\USSD\Screen;

class Welcome extends Screen
{

    /**
     * Add message to the screen
     *
     * @return string
     */
    protected function message(): string
    {
        return "";
    }

    /**
     * Add options to the screen
     * @return array
     */
    protected function options(): array
    {
        return [

        ];
    }

    /**
    * Previous screen
    * return Screen $screen
    */
    public function previous(): Screen
    {
        return new \App\USSD\Screens\Welcome($this->request);
    }

    /**
     * Execute the selected option/action
     *
     * @return mixed
     */
    protected function execute()
    {
        // TODO: Implement execute() method.
        $this->addPayload('senders_location', $this->value());
        return (new Location($this->request))->render();
    }
}
