<?php


namespace App\USSD\Screens\Farmers;

use App\Models\OrderRequest;
use App\Models\Waypoint;
use Faakolore\USSD\Models\Session;
use Faakolore\USSD\Screen;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class ConfirmRequest extends Screen
{

    /**
     * Add message to the screen
     *
     * @return string
     */
    protected function message(): string
    {
        return "You've requested for a truck to convey "
                .$this->payload('quantity')
                ." ".$this->load_type()
                ." of "
                .$this->payload('load_type')
                ." from "
                .$this->payload('senders_location')
                ." to "
                .$this->payload('destination');
    }

    /**
     * Add options to the screen
     * @return array
     */
    protected function options(): array
    {
        return [
            "Confirm",
            "Cancel"
        ];
    }

    /**
    * Previous screen
    * return Screen $screen
    */
    public function previous(): Screen
    {
        return new Quantity($this->request);
    }

    /**
     * Execute the selected option/action
     *
     * @return string
     */
    protected function execute(): string
    {
        if ($this->value()==='Cancel'){
            $this->addPayload('cancel',$this->value());

            return (new RequestCancelled($this->request))->render();
            //throw new UssdException($this->request,'Your order has been cancelled successfully, See you again');
        }else{
            $this->addPayload('confirm',$this->value());

//            $order = (new OrderRequest)::create([
//                'userable_id'=>Session::recentSessionByPhone($this->request->msisdn),
//                'userable_type'=>Session::class,
//                'location' => $this->payload('senders_location'),
//                'destination'=> $this->payload('destination'),
//                'quantity' => $this->payload('quantity'),
//                'load_type' => $this->payload('load_type'),
//                'platform' => 'ussd',
//            ]);

            $order = Session::findBySessionId($this->request->session)
                ->where('updated_at', '>=', now()->subMinutes(config('ussd.session.last_activity_minutes')))
                ->latest()->first()
                ->orders()->create([
                'quantity' => $this->payload('quantity'),
                'load_type' => $this->payload('load_type'),
                'platform' => 'ussd',
            ]);

            $pickup = new Waypoint([
                'order_request_id' => $order->id,
                'type' => 'pickup',
                'name'=>  $this->payload('senders_location'),
            ]);
//            $pickup->coordinates = new Point($input['lat'],$input['lng']);
            $pickup->save();

            $dropoff = new Waypoint([
                'order_request_id' => $order->id,
                'type' => 'dropoff',
                'name' => $this->payload('destination'),
            ]);
//            $dropoff->coordinates = new Point($input['latD'],$input['lngD']);
            $dropoff->save();

            return (new SuccessfulRequest($this->request))->render();
        }
    }

    protected function load_type(): string
    {
        $load_type ="";
        if ($this->payload('load_type')=="Grains"){
            $load_type='bag(s)';
        }elseif ($this->payload('load_type')=='Tubers'){
            $load_type='pieces';
        }elseif ($this->payload('load_type')==='Vegetables and Fruits'){
            $load_type='crate(s)';
        }
        return $load_type;
    }
}
