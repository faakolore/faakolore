<?php

namespace App\Notifications;

use App\Models\PhoneVerification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use NotificationChannels\Hubtel\HubtelChannel;
use NotificationChannels\Hubtel\HubtelMessage;

class VerifyPhoneNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable): array
    {
        return [HubtelChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toSMS($notifiable):HubtelMessage
    {
        $code = $this->buildVerifyRequest($notifiable);
        return (new HubtelMessage)
            ->from(config('app.name'))
            ->to($notifiable->getPhoneForVerification())
            ->content('Your verification code is '.$code);
    }

    public function buildVerifyRequest($notifiable): int
    {
        $code = rand(10000,99999);
        PhoneVerification::createVerifyRequest(
            $notifiable->getPhoneForVerification(),
            $code,
            HubtelChannel::class
        );
        return $code;
    }
}
