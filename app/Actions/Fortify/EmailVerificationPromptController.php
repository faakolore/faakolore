<?php

namespace App\Actions\Fortify;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Laravel\Fortify\Contracts\VerifyEmailViewResponse;

class EmailVerificationPromptController extends Controller
{
    /**
     * Display the email verification prompt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function __invoke(Request $request): \Illuminate\Http\RedirectResponse
    {
        return $request->user()->guard(['web'])->hasVerifiedPhone()
                && $request->user()->hasVerifiedEmail()
                    ? redirect()->intended(config('fortify.home'))
                    : app(VerifyEmailViewResponse::class);
    }
}
