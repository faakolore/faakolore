<?php


namespace App\Utilities\Contracts;


interface MustVerifyPhone
{
    /**
     * Determine if the user has verified their Phone number.
     *
     * @return bool
     */
    public function hasVerifiedPhone(): bool;

    /**
     * Mark the given user's Phone as verified.
     *
     * @return bool
     */
    public function markPhoneAsVerified(): bool;

    /**
     * Send the Phone verification notification.
     *
     * @return void
     */
    public function sendPhoneVerificationNotification();


    /**
     * Get the Phone number that should be used for verification.
     *
     * @return string
     */
    public function getPhoneForVerification(): string;
}
