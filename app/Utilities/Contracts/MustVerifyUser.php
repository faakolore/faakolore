<?php


namespace App\Utilities\Contracts;


use Illuminate\Contracts\Auth\MustVerifyEmail;

interface MustVerifyUser extends MustVerifyEmail, MustVerifyPhone
{

    /**
     * Send the Mail verification notification.
     *
     * @return void
     */
    public function sendMailVerificationNotification();
}
