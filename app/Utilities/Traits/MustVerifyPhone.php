<?php


namespace App\Utilities\Traits;


use App\Notifications\VerifyMail;
use App\Notifications\VerifyPhoneNotification;

trait MustVerifyPhone
{

    /**
     * Determine if the user has verified their phone number.
     *
     * @return bool
     */
    public function hasVerifiedPhone(): bool
    {
        return ! is_null($this->phone_verified_at);
    }

    /**
     * Mark the given user's phone as verified.
     *
     * @return bool
     */
    public function markPhoneAsVerified(): bool
    {
        return $this->forceFill([
            'phone_verified_at' => $this->freshTimestamp(),
        ])->save();
    }

    /**
     * Send the phone verification notification.
     *
     * @return void
     */
    public function sendPhoneVerificationNotification()
    {
        $this->notify(new VerifyPhoneNotification);
    }

    /**
     * Send the mail verification notification.
     *
     * @return void
     */
    public function sendMailVerificationNotification()
    {
        $this->notify(new VerifyMail);
    }

    /**
     * Get the phone number that should be used for verification.
     *
     * @return string
     */
    public function getPhoneForVerification(): string
    {
        return $this->phone;
    }

}
