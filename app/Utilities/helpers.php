<?php
//if (! function_exists('env')) {
//    function env($key, $default = null) {
//        // ...
//    }
//}

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

if (! function_exists('ghanapostGPS')) {
    /**
     * @param string $address
     *
     * @return false|string
     */
    function ghanapostGPS(string $address)
    {
        if (Cache::has($address)){
            return Cache::get($address);
        }
        else{
            $response = Http::asForm()
                ->timeout(0)
                ->acceptJson()
                ->post('https://ghpgps.herokuapp.com', [
                    'address' => $address
                ]);
//        return $response->collect()->flatten(2)->toArray();
//        return $response->collect()->flatten($array_depth)->toArray();
            Cache::put($address,$response->collect());
            return $response->collect();
        }
    }
}

    if (! function_exists('baseUrl')) {
        /**
         * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
         */
        function baseUrl() {
            return config('app.base_url');
        }
    }

    if (! function_exists('adminUrl')) {
        function adminUrl(): string
        {
            return config('app.admin_subdomain') . '.' . baseUrl();
        }
    }
    if (! function_exists('driverUrl')) {
        function driverUrl():string
        {
            return config('app.driver_subdomain'). '.' . baseUrl();
        }
    }
    if (! function_exists('warehouseUrl')) {
        function warehouseUrl() :string
        {
            return config('app.warehouse_subdomain'). '.' . baseUrl();
        }
    }

    if (! function_exists('whoCalled')) {
        /**
         *  Which guard made this request
         * @param $guards
         * @return string
         */
        function whoCalled($guards): string
        {
            foreach ($guards as $guard) {
                switch ($guard) {
                    case 'carriers':
                        return route('carriers.login');
                    case 'partners':
                        return route('partner.login');
                    case 'admins':
                        return route('admin.login');
                    default:
                        return route('login');
                }
            }
        }
    }

        if (! function_exists('validateDigitalAddress')) {

            /**
             * @param string $value
             * @return bool
             */
            function validateDigitalAddress(string $value): bool
            {
                $validator = false;
                if ((mb_strlen($value) == 9 || mb_strlen($value)==10 ) && is_numeric(mb_substr($value, -4)) == true
                    && is_string(mb_substr($value, 0)) == true) {
                    $last_digits = mb_substr($value, -4);
                    $first_char = mb_substr($value, 0);
                    $v1 = Validator::make([$last_digits, $first_char],
                        ["last_digits" => "numeric", 'first_char' => 'string']);
                    if ($v1->fails()) {
                        $validator = false;
                    } else {
                        $validator = true;
                    }
                }
                return $validator;
            }
    }

        if (!function_exists('convertAddress')){
            /**
             * @param \Illuminate\Support\Collection $GhanaPostGPS
             * @return string
             */
            function ConvertAddress(Collection $GhanaPostGPS): string
            {
                $address = $GhanaPostGPS['data']['Table'][0];
                return "Town: ".$address['Area']
                    .", "
                    ."Street: ".$address['Street']
                    .", "
                    ."District/Municipal: ".$address['District']
                    .", "
                    ."Region: ".$address['Region']
                    .", "
                    ."Digital Address: ".$address['GPSName'];
            }
        }

        if (!function_exists('miniAddress')){
            /**
             * @param \Illuminate\Support\Collection $GhanaPostGPS
             * @return string
             */
            function miniAddress(Collection $GhanaPostGPS):string
            {
                $address = $GhanaPostGPS['data']['Table'][0];
                return $address['Area']
                    .", "
                    .$address['Street']
                    .", "
                    .$address['District']
                    .", "
                    .$address['Region']." region, "
                    ."with digital address of ".$address['GPSName'];
            }
        }

if(!function_exists('loadType')){
    /**
     * @param $payload
     * @return string
     */
    function loadType($payload): string
    {
        $load_type ="";
        if (mb_strtolower($payload) == "grains"){
            $load_type='bag(s)';
        }elseif (mb_strtolower($payload) == 'tubers'){
            $load_type='pieces';
        }elseif (mb_strtolower($payload) ==='fruits'){
            $load_type='crate(s)';
        }
        return $load_type;
    }
}

