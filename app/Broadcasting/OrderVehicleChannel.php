<?php

namespace App\Broadcasting;

use App\Models\User;
use App\Models\OrderRequest;

class OrderVehicleChannel
{
    /**
     * Create a new channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Authenticate the user's access to the channel.
     *
     * @param User $user
     * @param OrderRequest $orderRequest
     * @return bool
     */
    public function join(User $user, OrderRequest $orderRequest): bool
    {
        return $user->id === $orderRequest->userable()->id;
    }
}
