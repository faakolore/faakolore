<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\VerifyCarrierRegistration
 *
 * @property int $id
 * @property int $carrier_id
 * @property int $vehicle
 * @property int $lanes
 * @property int $proof_of_documents
 * @property int $completed
 * @property int $verified
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Carrier $carrier
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyCarrierRegistration newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyCarrierRegistration newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyCarrierRegistration query()
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyCarrierRegistration whereCarrierId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyCarrierRegistration whereCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyCarrierRegistration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyCarrierRegistration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyCarrierRegistration whereLanes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyCarrierRegistration whereProofOfDocuments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyCarrierRegistration whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyCarrierRegistration whereVehicle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VerifyCarrierRegistration whereVerified($value)
 * @mixin \Eloquent
 */
class VerifyCarrierRegistration extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'vehicle', 'lanes', 'proof_of_documents', 'completed','verified'
    ];

    /**
     * @return BelongsTo
     */
    public function carrier(): BelongsTo
    {
        return $this->belongsTo(Carrier::class);
    }
}
