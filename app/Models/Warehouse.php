<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;




/**
 * App\Models\Warehouse
 *
 * @property int $id
 * @property int $partner_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Insurance[] $insurance
 * @property-read int|null $insurance_count
 * @property-read \App\Models\Partner $provider
 * @method static \Database\Factories\WarehouseFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse query()
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse wherePartnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Warehouse whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Warehouse extends Model
{
    use HasFactory;


    /**
     * This return the owner/provider of the warehouse
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider():BelongsTo
    {
        return $this->belongsTo(Partner::class);
    }
    /**
     * @return MorphMany
     */
    public function insurance():MorphMany
    {
        return $this->morphMany(Insurance::class,'insurable');
    }

}
