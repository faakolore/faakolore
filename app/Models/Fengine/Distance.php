<?php

namespace App\Models\Fengine;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Fengine\Distance
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Distance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Distance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Distance query()
 * @method static \Illuminate\Database\Eloquent\Builder|Distance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Distance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Distance whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Distance extends Model
{
    use HasFactory;
}
