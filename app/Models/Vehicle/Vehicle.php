<?php

namespace App\Models\Vehicle;

use App\Models\Carrier;
use App\Models\Insurance;
use App\Models\Vehicle\RoadWorthiness;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * App\Models\Vehicle\Vehicle
 *
 * @property int $id
 * @property string|null $partnable_type
 * @property int|null $partnable_id
 * @property int $body_type
 * @property int|null $type_by_size
 * @property string|null $year
 * @property string $license_plate
 * @property int|null $max_load
 * @property string|null $unit
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Carrier $carrier
 * @property-read \Illuminate\Database\Eloquent\Collection|Insurance[] $insurance
 * @property-read int|null $insurance_count
 * @property-read Model|\Eloquent $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|RoadWorthiness[] $roadworthiness
 * @property-read int|null $roadworthiness_count
 * @property-read \App\Models\Vehicle\VehicleType $vehicleType
 * @method static \Database\Factories\Vehicle\VehicleFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle query()
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereBodyType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereLicensePlate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereMaxLoad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle wherePartnableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle wherePartnableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereTypeBySize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereYear($value)
 * @mixin \Eloquent
 */
class Vehicle extends Model
{
    use HasFactory;
    protected $fillable = [
        'license_plate','year',
    ];

    /**
     * @return MorphTo
     */
    public function owner(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return BelongsTo
     */
    public function carrier(): BelongsTo
    {
        return $this->belongsTo(Carrier::class);
    }

//    /**
//     * @return HasMany
//     */
//    public function insurance():HasMany
//    {
//        return $this->hasMany(Insurance::class);
//    }


    /**
     * @return MorphMany
     */
    public function insurance():MorphMany
    {
        return $this->morphMany(Insurance::class,'insurable');
    }
    /**
     * @return HasMany
     */
    public function roadworthiness():HasMany
    {
        return $this->hasMany(RoadWorthiness::class);
    }

    /**
     * @return BelongsTo
     */
    public function vehicleType(): BelongsTo
    {
        return $this->belongsTo(VehicleType::class);
    }
}
