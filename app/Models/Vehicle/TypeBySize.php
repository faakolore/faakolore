<?php

namespace App\Models\Vehicle;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\Vehicle\TypeBySize
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vehicle\BodyType[] $bodyType
 * @property-read int|null $body_type_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vehicle\VehicleClassification[] $classification
 * @property-read int|null $classification_count
 * @method static \Database\Factories\Vehicle\TypeBySizeFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeBySize newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeBySize newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeBySize query()
 * @method static \Illuminate\Database\Eloquent\Builder|TypeBySize whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeBySize whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeBySize whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeBySize whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeBySize whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TypeBySize whereUrl($value)
 * @mixin \Eloquent
 */
class TypeBySize extends Model
{
    use HasFactory;

    /**
     * @return BelongsToMany
     */
    public function classification(): BelongsToMany
    {
        return $this->belongsToMany(VehicleClassification::class)->using(VehicleType::class);
    }

    /**
     * @return BelongsToMany
     */
    public function bodyType(): BelongsToMany
    {
        return $this->belongsToMany(BodyType::class)->using(VehicleType::class);
    }
}
