<?php

namespace App\Models\Vehicle;

use App\Models\ImageHandler;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * App\Models\Vehicle\RoadWorthiness
 *
 * @property int $id
 * @property int $vehicle_id
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \App\Models\Vehicle\Vehicle $vehicle
 * @method static \Database\Factories\Vehicle\RoadWorthinessFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|RoadWorthiness newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RoadWorthiness newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RoadWorthiness query()
 * @method static \Illuminate\Database\Eloquent\Builder|RoadWorthiness whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoadWorthiness whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoadWorthiness wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoadWorthiness whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RoadWorthiness whereVehicleId($value)
 * @mixin \Eloquent
 */
class RoadWorthiness extends Model implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    protected $fillable = [
        'path'
    ];
    /**
     * @return BelongsTo
     */
    public function vehicle(): BelongsTo
    {
        return $this->belongsTo(Vehicle::class);
    }

    public function registerMediaCollections() :void
    {

        $this
            ->addMediaCollection('road-worthiness')
            ->singleFile();
//            ->acceptsMimeTypes(['image/jpg','image/png','image/jpeg']);
    }

}
