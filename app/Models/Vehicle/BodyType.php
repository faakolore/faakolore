<?php

namespace App\Models\Vehicle;

use Eloquent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\Vehicle\BodyType
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vehicle\VehicleClassification[] $classification
 * @property-read int|null $classification_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vehicle\TypeBySize[] $typeBySize
 * @property-read int|null $type_by_size_count
 * @method static \Database\Factories\Vehicle\BodyTypeFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|BodyType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BodyType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BodyType query()
 * @method static \Illuminate\Database\Eloquent\Builder|BodyType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BodyType whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BodyType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BodyType whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BodyType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BodyType whereUrl($value)
 * @mixin Eloquent
 */
class BodyType extends Model
{
    use HasFactory;

    /**
     * @return BelongsToMany
     */
    public function classification(): BelongsToMany
    {
        return $this->belongsToMany(VehicleClassification::class)->using(VehicleType::class);
    }

    /**
     * @return BelongsToMany
     */
    public function typeBySize(): BelongsToMany
    {
        return $this->belongsToMany(TypeBySize::class)->using(VehicleType::class);
    }

}
