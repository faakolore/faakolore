<?php

namespace App\Models\Vehicle;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * App\Models\Vehicle\VehicleType
 *
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|\Spatie\MediaLibrary\MediaCollections\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vehicle\Vehicle[] $vehicle
 * @property-read int|null $vehicle_count
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $vehicle_classification_id
 * @property int $body_type_id
 * @property int $type_by_size_id
 * @property string $name
 * @property string $image
 * @property string $bucket_size
 * @property float $payload_capacity
 * @property float $GVW
 * @property float $towing_capacity
 * @property float $max_mileage
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType whereBodyTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType whereBucketSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType whereGVW($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType whereMaxMileage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType wherePayloadCapacity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType whereTowingCapacity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType whereTypeBySizeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleType whereVehicleClassificationId($value)
 */
class VehicleType extends Model implements HasMedia
{
    use InteractsWithMedia;

    public $incrementing = true;

    public $fillable = ['image'];


    /**
     * @return HasMany
     */
    public function vehicle(): HasMany
    {
        return $this->hasMany(Vehicle::class);
    }
}
