<?php

namespace App\Models\Vehicle;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * App\Models\Vehicle\VehicleClassification
 *
 * @property int $id
 * @property string $class
 * @property string $notation
 * @property string $description
 * @property int $GVWR
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vehicle\BodyType[] $bodyType
 * @property-read int|null $body_type_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vehicle\TypeBySize[] $typeBySize
 * @property-read int|null $type_by_size_count
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleClassification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleClassification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleClassification query()
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleClassification whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleClassification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleClassification whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleClassification whereGVWR($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleClassification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleClassification whereNotation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|VehicleClassification whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class VehicleClassification extends Model
{
    use HasFactory;


    /**
     * @return BelongsToMany
     */
    public function bodyType(): BelongsToMany
    {
        return $this->belongsToMany(BodyType::class)->using(VehicleType::class);
    }

    /**
     * @return BelongsToMany
     */
    public function typeBySize(): BelongsToMany
    {
        return $this->belongsToMany(TypeBySize::class)->using(VehicleType::class);
    }

}
