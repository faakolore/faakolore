<?php

namespace App\Models;

use Eloquent;
use Grimzy\LaravelMysqlSpatial\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Carbon;


/**
 * App\Models\Waypoint
 *
 * @property int $id
 * @property string $type
 * @property string $order_request_id
 * @property string $address
 * @property string|null $alias
 * @property string|null $name
 * @property string|null $full_address
 * @property string $coordinates
 * @property string|null $way_stops
 * @property string|null $message
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Location|null $location
 * @property-read OrderRequest $order
 * @method static Builder|Waypoint comparison($geometryColumn, $geometry, $relationship)
 * @method static Builder|Waypoint contains($geometryColumn, $geometry)
 * @method static Builder|Waypoint crosses($geometryColumn, $geometry)
 * @method static Builder|Waypoint disjoint($geometryColumn, $geometry)
 * @method static Builder|Waypoint distance($geometryColumn, $geometry, $distance)
 * @method static Builder|Waypoint distanceExcludingSelf($geometryColumn, $geometry, $distance)
 * @method static Builder|Waypoint distanceSphere($geometryColumn, $geometry, $distance)
 * @method static Builder|Waypoint distanceSphereExcludingSelf($geometryColumn, $geometry, $distance)
 * @method static Builder|Waypoint distanceSphereValue($geometryColumn, $geometry)
 * @method static Builder|Waypoint distanceValue($geometryColumn, $geometry)
 * @method static Builder|Waypoint doesTouch($geometryColumn, $geometry)
 * @method static Builder|Waypoint equals($geometryColumn, $geometry)
 * @method static Builder|Waypoint intersects($geometryColumn, $geometry)
 * @method static Builder|Waypoint newModelQuery()
 * @method static Builder|Waypoint newQuery()
 * @method static Builder|Waypoint orderByDistance($geometryColumn, $geometry, $direction = 'asc')
 * @method static Builder|Waypoint orderByDistanceSphere($geometryColumn, $geometry, $direction = 'asc')
 * @method static Builder|Waypoint orderBySpatial($geometryColumn, $geometry, $orderFunction, $direction = 'asc')
 * @method static Builder|Waypoint overlaps($geometryColumn, $geometry)
 * @method static Builder|Waypoint query()
 * @method static Builder|Waypoint whereAddress($value)
 * @method static Builder|Waypoint whereAlias($value)
 * @method static Builder|Waypoint whereCoordinates($value)
 * @method static Builder|Waypoint whereCreatedAt($value)
 * @method static Builder|Waypoint whereFullAddress($value)
 * @method static Builder|Waypoint whereId($value)
 * @method static Builder|Waypoint whereMessage($value)
 * @method static Builder|Waypoint whereName($value)
 * @method static Builder|Waypoint whereOrderRequestId($value)
 * @method static Builder|Waypoint whereType($value)
 * @method static Builder|Waypoint whereUpdatedAt($value)
 * @method static Builder|Waypoint whereWayStops($value)
 * @method static Builder|Waypoint within($geometryColumn, $polygon)
 * @mixin Eloquent
 */
class Waypoint extends Model
{
    use HasFactory;
    use SpatialTrait;

    /**
     * @var string[]
     */
    protected $fillable = [
        'address',
        'type',
        'message',
        'alias',
        'name',
        'full_address',
        'order_request_id',
    ];

    protected array $spatialFields = [
        'coordinates',
        'way_stops',
    ];
    /**
     * @return BelongsTo
     */
    public function order(): BelongsTo
    {
        return $this->belongsTo(OrderRequest::class);
    }

    /**
     * @return HasOne
     */
    public function location(): HasOne
    {
        return $this->hasOne(Location::class);
    }

    /**
     * @param $value
     */
    public function setTypeAttribute($value)
    {
        if (mb_strtolower($value)=='dropoff'){
            $this->attributes['type'] = config('constants.WAYPOINT.DROPOFF');
        }
        elseif (mb_strtolower($value)=='pickup'){
            $this->attributes['type'] = config('constants.WAYPOINT.PICKUP');
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getTypeAttribute($value): string
    {
        $string = 'UNDEFINED';
        if ($value== config('constants.WAYPOINT.PICKUP')){
            $string = 'PICKUP';
        }
        elseif($value== config('constants.WAYPOINT.DROPOFF'))
        {
            $string =  'DROPOFF';
        }
        return $string;
    }

}
