<?php

namespace App\Models;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\OperatingLane
 *
 * @property int $id
 * @property int $carrier_id
 * @property string $lane
 * @property string|null $city
 * @property string|null $coordinates
 * @property int|null $radius
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Carrier $carrier
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane comparison($geometryColumn, $geometry, $relationship)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane contains($geometryColumn, $geometry)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane crosses($geometryColumn, $geometry)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane disjoint($geometryColumn, $geometry)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane distance($geometryColumn, $geometry, $distance)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane distanceExcludingSelf($geometryColumn, $geometry, $distance)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane distanceSphere($geometryColumn, $geometry, $distance)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane distanceSphereExcludingSelf($geometryColumn, $geometry, $distance)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane distanceSphereValue($geometryColumn, $geometry)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane distanceValue($geometryColumn, $geometry)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane doesTouch($geometryColumn, $geometry)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane equals($geometryColumn, $geometry)
 * @method static \Database\Factories\OperatingLaneFactory factory(...$parameters)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane intersects($geometryColumn, $geometry)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane newModelQuery()
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane newQuery()
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane orderByDistance($geometryColumn, $geometry, $direction = 'asc')
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane orderByDistanceSphere($geometryColumn, $geometry, $direction = 'asc')
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane orderBySpatial($geometryColumn, $geometry, $orderFunction, $direction = 'asc')
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane overlaps($geometryColumn, $geometry)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane query()
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane whereCarrierId($value)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane whereCity($value)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane whereCoordinates($value)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane whereCreatedAt($value)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane whereId($value)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane whereLane($value)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane whereRadius($value)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane whereUpdatedAt($value)
 * @method static \Grimzy\LaravelMysqlSpatial\Eloquent\Builder|OperatingLane within($geometryColumn, $polygon)
 * @mixin \Eloquent
 */
class OperatingLane extends Model
{
    use HasFactory;
    use SpatialTrait;

    protected $fillable = [
        'carrier_id','city','radius', 'lane',
    ];

    protected array $spatialFields = [
        'coordinates',
    ];

    /**
     * @return BelongsTo
     */
    public function carrier(): BelongsTo
    {
        return $this->belongsTo(Carrier::class);
    }

    /**
     * @param $value
     */
    public function setLaneAttribute($value)
    {
        if (mb_strtolower($value)=='regional'){
            $this->attributes['lane'] = config('constants.OPERATING_LANE.REGIONAL');
        }
        elseif (mb_strtolower($value)== 'nationwide'){
            $this->attributes['lane'] = config('constants.OPERATING_LANE.NATIONWIDE');
        }
        elseif (mb_strtolower($value)=='continental'){
            $this->attributes['lane'] = config('constants.OPERATING_LANE.CONTINENTAL');
        }
    }

    /**
     * @param $value
     * @return string
     */
    public function getLaneAttribute($value): string
    {
        $string = '';
        if ($value == config('constants.OPERATING_LANE.REGIONAL')){
            $string = 'REGIONAL';
        }elseif ($value == config('constants.OPERATING_LANE.NATIONWIDE')){
            $string = 'NATIONWIDE';
        }elseif( $value == config('constants.OPERATING_LANE.CONTINENTAL')){
            $string = 'CONTINENTAL';
        }
        return $string;
    }
}
