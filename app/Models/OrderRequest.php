<?php

namespace App\Models;

use Database\Factories\OrderRequestFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Str;

/**
 * App\Models\OrderRequest
 *
 * @property string $id
 * @property string $userable_type
 * @property int $userable_id
 * @property int $quantity
 * @property string $load_type
 * @property string $platform
 * @property int|null $carrier_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Carrier|null $carrier
 * @property-read Model|\Eloquent $userable
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Waypoint[] $waypoint
 * @property-read int|null $waypoint_count
 * @method static \Database\Factories\OrderRequestFactory factory(...$parameters)
 * @method static Builder|OrderRequest newModelQuery()
 * @method static Builder|OrderRequest newQuery()
 * @method static Builder|OrderRequest query()
 * @method static Builder|OrderRequest whereCarrierId($value)
 * @method static Builder|OrderRequest whereCreatedAt($value)
 * @method static Builder|OrderRequest whereId($value)
 * @method static Builder|OrderRequest whereLoadType($value)
 * @method static Builder|OrderRequest wherePlatform($value)
 * @method static Builder|OrderRequest whereQuantity($value)
 * @method static Builder|OrderRequest whereUpdatedAt($value)
 * @method static Builder|OrderRequest whereUserableId($value)
 * @method static Builder|OrderRequest whereUserableType($value)
 * @mixin Eloquent
 */
class OrderRequest extends Model
{
    use HasFactory;


    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;


    protected $fillable = [
        'load_type', 'quantity','platform'
    ];

    /**
     * @return MorphTo
     */
    public function userable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return HasMany
     */
    public function waypoint(): HasMany
    {
        return $this->hasMany(Waypoint::class);
    }

    /**
     * @return BelongsTo
     */
    public function carrier(): BelongsTo
    {
        return $this->belongsTo(Carrier::class);
    }

    public function confirmOrderMessage(): string
    {
        return "You've requested for a truck to convey "
            .$this->quantity
            ." ".loadType($this->load_type)
            ." of "
            .$this->load_type
            ." from "
            .$this->waypoint()->whereType(1)->pluck('address')->first()
            ." to "
            .$this->waypoint()->whereType(0)->pluck('address')->first();
    }
}
