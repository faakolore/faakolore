<?php

namespace App\Models;

use App\Models\Vehicle\Vehicle;
use App\Notifications\ResetPasswordNotification;
use App\Notifications\VerifyMail;
use App\Utilities\Contracts\MustVerifyPhone as MustVerifyPhoneContract;
use App\Utilities\Contracts\MustVerifyUser;
use App\Utilities\Traits\MustVerifyPhone;
use Database\Factories\CarrierFactory;
use Eloquent;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Sanctum\PersonalAccessToken;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection;
use Spatie\MediaLibrary\MediaCollections\Models\Media;


/**
 * App\Models\Carrier
 *
 * @property int $id
 * @property string $uuid
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property Carbon|null $phone_verified_at
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $two_factor_secret
 * @property string|null $two_factor_recovery_codes
 * @property string|null $remember_token
 * @property int|null $current_team_id
 * @property string|null $profile_photo_path
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|\App\Models\Account[] $accounts
 * @property-read int|null $accounts_count
 * @property-read string $profile_photo_url
 * @property-read \App\Models\VerifyCarrierRegistration|null $hasVerifiedRegistration
 * @property-read \App\Models\License|null $license
 * @property-read MediaCollection|Media[] $media
 * @property-read int|null $media_count
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \App\Models\OperatingLane|null $operatingLanes
 * @property-read Collection|PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @property-read Vehicle|null $vehicle
 * @method static \Database\Factories\CarrierFactory factory(...$parameters)
 * @method static Builder|Carrier newModelQuery()
 * @method static Builder|Carrier newQuery()
 * @method static Builder|Carrier query()
 * @method static Builder|Carrier whereCreatedAt($value)
 * @method static Builder|Carrier whereCurrentTeamId($value)
 * @method static Builder|Carrier whereEmail($value)
 * @method static Builder|Carrier whereEmailVerifiedAt($value)
 * @method static Builder|Carrier whereId($value)
 * @method static Builder|Carrier whereName($value)
 * @method static Builder|Carrier wherePassword($value)
 * @method static Builder|Carrier wherePhone($value)
 * @method static Builder|Carrier wherePhoneVerifiedAt($value)
 * @method static Builder|Carrier whereProfilePhotoPath($value)
 * @method static Builder|Carrier whereRememberToken($value)
 * @method static Builder|Carrier whereTwoFactorRecoveryCodes($value)
 * @method static Builder|Carrier whereTwoFactorSecret($value)
 * @method static Builder|Carrier whereUpdatedAt($value)
 * @method static Builder|Carrier whereUuid($value)
 * @mixin Eloquent
 */
class Carrier extends Authenticatable implements HasMedia, MustVerifyUser
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use InteractsWithMedia;
    use MustVerifyPhone;

    /**
     * @var string
     */
    public string $guard = 'carriers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'uuid'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }


    /**
     * This registers the media collections for the carrier
     * @return void
     */
    public function registerMediaCollections() : void
    {
        $this
            ->addMediaCollection('avatar')
            ->singleFile()
            ->registerMediaConversions(function (Media $media) {
                $this
                    ->addMediaConversion('thumb-100')
                    ->width(100)
                    ->height(100);

                $this
                    ->addMediaConversion('thumb-200')
                    ->width(200)
                    ->height(200);
            });

        $this
            ->addMediaCollection('passport')
            ->singleFile()
            ->withResponsiveImages()
            ->acceptsMimeTypes(['image/jpg','image/png','image/jpeg'])
            ->registerMediaConversions(function (Media $media){
                $this
                    ->addMediaConversion('passport-100')
                    ->width(100)
                    ->height(100);
                $this
                    ->addMediaConversion('passport-200')
                    ->width(200)
                    ->height(200);
            });

    }


    /**
     * @return string
     */
    public function routeNotificationForSMS(): string
    {
        return $this->phone; // where phone is a column in carriers table;
    }
    /**
     * @return MorphOne
     */
    public function vehicle(): MorphOne
    {
       return $this->morphOne(Vehicle::class, 'partnable');
    }


    /**
     * @return HasOne
     */
    public function license(): HasOne
    {
        return $this->hasOne(License::class);
    }

    /**
     * @return MorphMany
     */
    public function accounts():MorphMany
    {
        return $this->morphMany(Account::class,'accountable');
    }

    /**
     * @return HasOne
     */
    public function hasVerifiedRegistration(): HasOne
    {
        return $this->HasOne(VerifyCarrierRegistration::class);
    }

    /**
     * @return HasOne
     */
    public function operatingLanes(): HasOne
    {
        return $this->hasOne(OperatingLane::class);
    }

}
