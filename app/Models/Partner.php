<?php

namespace App\Models;

use App\Models\Vehicle\Vehicle;
use App\Notifications\ResetPasswordNotification;
use App\Utilities\Contracts\MustVerifyPhone as MustVerifyPhoneContract;
use App\Utilities\Contracts\MustVerifyUser;
use App\Utilities\Traits\MustVerifyPhone;
use Eloquent;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Sanctum\PersonalAccessToken;



/**
 * App\Models\Partner
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $phone
 * @property Carbon|null $email_verified_at
 * @property Carbon|null $phone_verified_at
 * @property string $password
 * @property string|null $two_factor_secret
 * @property string|null $two_factor_recovery_codes
 * @property string|null $remember_token
 * @property int|null $current_team_id
 * @property string|null $profile_photo_path
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|\App\Models\Account[] $accounts
 * @property-read int|null $accounts_count
 * @property-read Collection|Vehicle[] $fleet
 * @property-read int|null $fleet_count
 * @property-read string $profile_photo_url
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|PersonalAccessToken[] $tokens
 * @property-read int|null $tokens_count
 * @property-read Collection|\App\Models\Warehouse[] $warehouses
 * @property-read int|null $warehouses_count
 * @method static Builder|Partner newModelQuery()
 * @method static Builder|Partner newQuery()
 * @method static Builder|Partner query()
 * @method static Builder|Partner whereCreatedAt($value)
 * @method static Builder|Partner whereCurrentTeamId($value)
 * @method static Builder|Partner whereEmail($value)
 * @method static Builder|Partner whereEmailVerifiedAt($value)
 * @method static Builder|Partner whereId($value)
 * @method static Builder|Partner whereName($value)
 * @method static Builder|Partner wherePassword($value)
 * @method static Builder|Partner wherePhone($value)
 * @method static Builder|Partner wherePhoneVerifiedAt($value)
 * @method static Builder|Partner whereProfilePhotoPath($value)
 * @method static Builder|Partner whereRememberToken($value)
 * @method static Builder|Partner whereTwoFactorRecoveryCodes($value)
 * @method static Builder|Partner whereTwoFactorSecret($value)
 * @method static Builder|Partner whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Partner extends Authenticatable implements MustVerifyUser
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;
    use MustVerifyPhone;

    /**
     * @var string
     */
    public string $guard = 'partners';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','phone', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'phone_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];


    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * @return string
     */
    public function routeNotificationForSMS(): string
    {
        return $this->phone; // where phone is a column in partners table;
    }

    /**
     * @return MorphMany
     */
    public function fleet(): MorphMany
    {
        return $this->morphMany(Vehicle::class, 'partnable');
    }


    /**
     * @return HasMany
     */
    public function warehouses(): HasMany
    {
        return $this->hasMany(Warehouse::class);
    }
    /**
     * @return MorphMany
     */
    public function accounts():MorphMany
    {
        return $this->morphMany(Account::class,'accountable');
    }
}
