<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PhoneVerification
 *
 * @method static Builder|PhoneVerification newModelQuery()
 * @method static Builder|PhoneVerification newQuery()
 * @method static Builder|PhoneVerification query()
 * @mixin Eloquent
 * @property int $id
 * @property string $phone
 * @property string|null $code
 * @property string|null $channel
 * @property int $active
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static Builder|PhoneVerification whereActive($value)
 * @method static Builder|PhoneVerification whereChannel($value)
 * @method static Builder|PhoneVerification whereCode($value)
 * @method static Builder|PhoneVerification whereCreatedAt($value)
 * @method static Builder|PhoneVerification whereId($value)
 * @method static Builder|PhoneVerification wherePhone($value)
 * @method static Builder|PhoneVerification whereUpdatedAt($value)
 */
class PhoneVerification extends Model
{
    use HasFactory;

    protected $fillable = [
        'phone', 'code','channel'
    ];

    public static function createVerifyRequest($phoneNumber, $code, $channel=null)
    {
        return self::create([
           'phone' => $phoneNumber,
            'code'  => $code,
            'channel' => $channel
        ]);
    }

    public static function latestVerification($phoneNumber)
    {
        return self::wherePhone($phoneNumber)
//            ->orderBy('created_at', 'DESC')
//            ->where('active', 1)
            ->latest()
            ->first();
    }
}
