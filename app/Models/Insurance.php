<?php

namespace App\Models;

use App\Models\Vehicle\Vehicle;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * App\Models\Insurance
 *
 * @property int $id
 * @property string $insurable_type
 * @property int $insurable_id
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read Model|\Eloquent $insurable
 * @method static \Database\Factories\InsuranceFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Insurance newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Insurance newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Insurance query()
 * @method static \Illuminate\Database\Eloquent\Builder|Insurance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Insurance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Insurance whereInsurableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Insurance whereInsurableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Insurance whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Insurance whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Insurance extends Model
{
    use HasFactory;


    /**
     * @return MorphTo
     */
    public function insurable(): MorphTo
    {
        return $this->morphTo();
    }
}
