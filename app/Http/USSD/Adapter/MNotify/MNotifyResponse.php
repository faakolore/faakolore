<?php

namespace App\Http\USSD\Adapter\MNotify;

use Faakolore\USSD\Http\UssdResponseInterface;

use Faakolore\USSD\Screen;

class MNotifyResponse implements UssdResponseInterface
{

    public function respond(Screen $screen)
    {
        return $this->toJson($screen);
    }
}