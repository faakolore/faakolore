<?php

namespace App\Http\USSD\Adapter\Wigal;

use Faakolore\USSD\Http\UssdResponseInterface;

use Faakolore\USSD\Screen;

class WigalResponse implements UssdResponseInterface
{

    public function respond(Screen $screen)
    {
        return $screen;
    }
}
