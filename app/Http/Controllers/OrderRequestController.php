<?php

namespace App\Http\Controllers;

use App\Engine\Core\Core;
use App\Events\OrderVehicleStatus;
use App\Http\Requests\DigitalAddressRequest;
use App\Models\OrderRequest;
use App\Models\Waypoint;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class OrderRequestController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth','verified'])->except(['logout','destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Inertia\Response
     */
    public function index(): \Inertia\Response
    {
        return Inertia::render('RequestOrder/OrderPage')
            ->with('status','request-order');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Inertia\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): \Inertia\Response
    {
        $input = $request->all();

        Validator::make($input,[
            'pickup'=> ['required','address'],
            'dropoff'=>['required','address'],
            'load_type'=>['required','string'],
            'quantity'=>['required','numeric','min:1'],
            'lat'   => 'required|numeric',
            'latD'   => 'required|numeric',
            'lng'   => 'required|numeric',
            'lngD'   => 'required|numeric',
        ])->validate();

       $order = auth()->user()->orders()->create([
//            'pickup_id'=>  $request['pickup'],
//            'dropoff_id'=> $request['dropoff'],
            'load_type'=>$input['load_type'],
            'quantity'=>$input['quantity'],
            'platform'=>'web'
        ]);

       foreach ([$input['pickup']] as $pickupAddress){
           $pickup = new Waypoint([
               'order_request_id' => $order->id,
               'type' => 'pickup',
               'address'=>  $pickupAddress,
//               'message' => $input['message'],
           ]);
           $pickup->coordinates = new Point($input['lat'],$input['lng']);
           $pickup->save();
       }

       foreach ([$input['dropoff']] as $dropoffAddress){
           $dropoff = new Waypoint([
               'order_request_id' => $order->id,
               'type' => 'dropoff',
               'address' => $dropoffAddress,
//               'message' => $input['message'],
           ]);
           $dropoff->coordinates = new Point($input['latD'],$input['lngD']);
           $dropoff->save();
       }

//        GoogleMaps::load('distancematrix')
//            ->setParam([
//                'origins'       => ['6.6975237,-1.6291939'],
//                'destinations'  => ['7.583819099999999,-1.9371715'],
//                'mode' => 'driving',
//            ])
        $confirm = $order->confirmOrderMessage();
        $order = $order->id;
        return Inertia::render('RequestOrder/OrderPage',
            compact(['confirm','order']))
            ->with('status','confirm-order');
//        return redirect()->back()->with('status','confirm-order');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Inertia\Response
     */
    public function update(Request $request): \Inertia\Response
    {
        $order = $request->orderId;
        $requestVehicle = (new OrderRequest)->findOrFail($order);
        (new Core($requestVehicle))->execute();
//                OrderVehicleStatus::dispatch($requestVehicle);

        return Inertia::render('RequestOrder/OrderPage',compact('order'))
            ->with('status','search-truck');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrderRequest  $orderRequest
     * @return Response
     */
    public function show(OrderRequest $orderRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrderRequest  $orderRequest
     * @return Response
     */
    public function edit(OrderRequest $orderRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrderRequest  $orderRequest
     * @return Response
     */
    public function destroy(OrderRequest $orderRequest)
    {
        //
    }
}
