<?php

namespace App\Http\Controllers\Auth\Carriers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Rules\PhoneVerificationCode;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PhoneVerificationController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:carriers']);
    }
    /**
     * Display the email and phone verification prompt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        return $request->user()->guard(['carriers'])->hasVerifiedPhone()
        && $request->user()->guard(['carriers'])->hasVerifiedEmail()
            ? redirect()->intended(RouteServiceProvider::CARRIER_DASHBOARD)
            : Inertia::render('Auth/Carriers/VerifyEmail', ['status' => session('status')]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function verify(Request $request): RedirectResponse
    {
        if ($request->user()->guard(['carriers'])->hasVerifiedPhone()
            && $request->user()->guard(['carriers'])->hasVerifiedEmail()) {
            return redirect()->intended(RouteServiceProvider::CARRIER_DASHBOARD.'?verified=1');
        }

        $request->validate([
            'code' => ['required','numeric', new PhoneVerificationCode($request)]
        ]);
        if ($request->user()->guard(['carriers'])->markPhoneAsVerified()) {
            event(new Verified($request->user()));
        }

        return redirect()->intended(RouteServiceProvider::CARRIER_DASHBOARD.'?verified=1');
    }


    /**
     * Send a new phone verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        if ($request->user()->guard(['carriers'])->hasVerifiedPhone()
            && $request->user()->guard(['carriers'])->hasVerifiedEmail()) {
            return redirect()->intended(RouteServiceProvider::CARRIER_DASHBOARD);
        }

        $request->user()->guard(['carriers'])->sendPhoneVerificationNotification();

        return back()->with('status', 'verification-phone-link-sent');
    }
}
