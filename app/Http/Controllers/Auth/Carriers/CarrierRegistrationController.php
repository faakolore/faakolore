<?php

namespace App\Http\Controllers\Auth\Carriers;

use App\Http\Controllers\Controller;
use App\Models\OperatingLane;
use App\Models\Vehicle\BodyType;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;


class CarrierRegistrationController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */


    public function __construct()
    {
        $this->middleware(['auth:carriers','verified']);
    }

    /**
     * @return \Inertia\Response
     */
    public function createVehicle(): \Inertia\Response
    {
        $trucks = BodyType::all();
        $VR = request()->user()->hasVerifiedRegistration();
        return Inertia::render('Auth/Carriers/Vehicle',compact(['trucks','VR']));
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws ValidationException
     */
    public function storeVehicle(Request $request)
    {
        $input = $request->all();

        Validator::make($input,[
            'truck' => 'required|numeric|exists:body_types,id',
            //'max_load' => 'required|numeric',
            //'unit' => 'required|string',
            'license_plate' => 'required|alpha_dash|unique:vehicles',
            'license_number' => 'required|numeric|unique:licenses,number',
            'reference_number' => 'required|alpha_num|unique:licenses,reference',
            'expiry_date' => 'required|date'
        ])->validate();

        $carrier = Auth::guard('carriers')->user();
        $vehicle = $carrier->vehicle()->create([
            'body_type' => $input['truck'],
            'license_plate' => $input['license_plate'],
            //'max_load' => $input['max_load'],
            //'unit' => $input['unit'],
        ]);

        $license = $carrier->license()->create([
            'number' => $input['license_number'],
            'reference' => $input['reference_number'],
            'expiry' => $input['expiry_date']
        ]);

        /** @var $carrier */
        $carrier->hasVerifiedRegistration()->update([
            'vehicle' => true,
        ]);
        return redirect()->route('carriers.register.lanes');
    }

    /**
     * @return \Inertia\Response
     */
    public function createLanes(): \Inertia\Response
    {
        $VR = request()->user()->hasVerifiedRegistration();
        return Inertia::render('Auth/Carriers/OperatingLanes',compact('VR'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function storeLanes(Request $request): RedirectResponse
    {
        $input = $request->all();
        Validator::make($input, [
            'city' => 'required|address',
            'radius' => 'nullable|numeric',
            'lane' => 'required|string',
            'lat' => 'required|numeric',
            'lng' => 'required|numeric'
        ])->validate();

        $carrier = Auth::guard('carriers')->user();
        $operatingLane = new OperatingLane([
            'carrier_id' => $carrier->id,
            'city' => $input['city'],
            'radius' => $input['radius'],
            'lane' => mb_strtoupper($input['lane']),
        ]);

        $operatingLane->coordinates = new Point($input['lat'],$input['lng']);
        $operatingLane->save();

        $carrier->hasVerifiedRegistration()->update([
            'lanes'=>true,
        ]);

        return redirect()->route('carriers.register.proof');
    }

    /**
     * @return \Inertia\Response
     */
    public function createProof(): \Inertia\Response
    {
        $VR = request()->user()->hasVerifiedRegistration();
        return  Inertia::render('Auth/Carriers/ProofOfDocuments', compact('VR'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function storeProof(Request $request): RedirectResponse
    {
        Validator::make($request->all(),[
            'passport' => 'required|mimes:png,jpg,jpeg',
            'license' => 'required|mimes:png,jpg,jpeg',
            'worthiness' => 'required|mimes:png,jpg,pdf,jpeg',
//            'passport_photo' => 'required|mimes:png,jpg',
        ])->validate();

        $carrier = Auth::guard('carriers')->user();
        $passport = $request->file('passport')->store('passport','public');
//        $passport_photo = $request->file('passport_photo')->store('user+license','public');
        $license = $request->file('license')->store('license','public');
        $worthiness = $request->file('worthiness')->store('worthiness','public');
        $carrier->addMedia('storage/'.$passport)->toMediaCollection('passport');
//        $carrier->addMedia('storage/'.$passport_photo)->toMediaCollection('passport_photo');
        $carrier->license->addMedia('storage/'.$license)->toMediaCollection('carrier-license');
        $carrier->vehicle->roadworthiness->path = $worthiness;
        $carrier->hasVerifiedRegistration()->update([
            'proof_of_documents' => true,
        ]);
        return redirect()->route('carriers.register.completed');
    }

    /**
     * @return \Inertia\Response
     */
    public function createCompleted(): \Inertia\Response
    {
        $VR = request()->user()->hasVerifiedRegistration();
        return Inertia::render('Auth/Carriers/Completed',compact('VR'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function storeCompleted(Request $request): RedirectResponse
    {
        $carrier = Auth::guard('carriers')->user();
        $carrier->hasVerifiedRegistration()->update([
            'completed' => true,
            'verified' => true,
        ]);
        return redirect()->route('carriers.dashboard');
    }
}
