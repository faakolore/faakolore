<?php

namespace App\Http\Controllers\Auth\Carriers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Inertia\Inertia;

class EmailVerificationPromptController extends Controller
{
    /**
     * Display the email verification prompt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        return $request->user()->guard(['carriers'])->hasVerifiedPhone()
                && $request->user()->guard(['carriers'])->hasVerifiedEmail()
                    ? redirect()->intended(RouteServiceProvider::CARRIER_DASHBOARD)
                    : Inertia::render('Auth/Carriers/VerifyEmail', ['status' => session('status')]);
    }
}
