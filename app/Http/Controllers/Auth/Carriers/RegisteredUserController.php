<?php

namespace App\Http\Controllers\Auth\Carriers;

use App\Http\Controllers\Controller;
use App\Models\Carrier;
use App\Providers\RouteServiceProvider;
use App\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;
use Propaganistas\LaravelPhone\PhoneNumber;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Inertia\Response
     */
    public function create(): \Inertia\Response
    {
        return Inertia::render('Auth/Carriers/Register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        Validator::validate(['phone'=> $request['phone']],['phone'=>['required','digits:10','phone:GH']]);
        $request['phone'] = PhoneNumber::make($request['phone'],'GH')->formatE164();

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:carriers',
            'phone' => 'required|phone:GH|unique:carriers',
            'password' => 'required|string|confirmed|min:8',
        ]);

        $carrier = Carrier::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => PhoneNumber::make($request['phone'],'GH')->formatE164(),
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($carrier));

        Auth::guard('carriers')->login($carrier);

        return redirect(RouteServiceProvider::CARRIER_DASHBOARD);
    }
}
