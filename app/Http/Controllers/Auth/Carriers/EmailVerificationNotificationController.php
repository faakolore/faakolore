<?php

namespace App\Http\Controllers\Auth\Carriers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;

class EmailVerificationNotificationController extends Controller
{
    /**
     * Send a new email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        if ($request->user()->guard(['carriers'])->hasVerifiedPhone()
            && $request->user()->guard(['carriers'])->hasVerifiedEmail()) {
            return redirect()->intended(RouteServiceProvider::CARRIER_DASHBOARD);
        }

        $request->user()->guard(['carriers'])->sendMailVerificationNotification();

        return back()->with('status', 'verification-email-link-sent');
    }
}
