<?php

namespace App\Http\Controllers\Auth\Carriers;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\RedirectResponse;

class VerifyEmailController extends Controller
{
    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param EmailVerificationRequest $request
     * @return RedirectResponse
     */
    public function __invoke(EmailVerificationRequest $request): RedirectResponse
    {
        if ($request->user()->guard(['carriers'])->hasVerifiedPhone()
                && $request->user()->guard(['carriers'])->hasVerifiedEmail()) {
            return redirect()->intended(RouteServiceProvider::CARRIER_DASHBOARD.'?verified=1');
        }

        if ($request->user()->guard(['carriers'])->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        return redirect()->intended(RouteServiceProvider::CARRIER_DASHBOARD.'?verified=1');
    }
}
