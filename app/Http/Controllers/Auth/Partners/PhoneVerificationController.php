<?php

namespace App\Http\Controllers\Auth\Partners;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Rules\PhoneVerificationCode;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PhoneVerificationController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth:partners']);
    }
    /**
     * Display the email and phone verification prompt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        return $request->user()->guard(['partners'])->hasVerifiedPhone()
                && $request->user()->guard(['partners'])->hasVerifiedEmail()
            ? redirect()->intended(RouteServiceProvider::PARTNER_DASHBOARD)
            : Inertia::render('Auth/Partners/VerifyEmail', ['status' => session('status')]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function verify(Request $request): RedirectResponse
    {
        if ($request->user()->guard(['partners'])->hasVerifiedPhone()
            && $request->user()->guard(['partners'])->hasVerifiedEmail()) {
            return redirect()->intended(RouteServiceProvider::PARTNER_DASHBOARD.'?verified=1');
        }

        $request->validate([
            'code' => ['required','numeric', new PhoneVerificationCode($request)]
        ]);
        if ($request->user()->guard(['partners'])->markPhoneAsVerified()) {
            event(new Verified($request->user()));
        }

        return redirect()->intended(RouteServiceProvider::PARTNER_DASHBOARD.'?verified=1');
    }


    /**
     * Send a new phone verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        if ($request->user()->guard(['partners'])->hasVerifiedPhone()
            && $request->user()->guard(['partners'])->hasVerifiedEmail()) {
            return redirect()->intended(RouteServiceProvider::PARTNER_DASHBOARD);
        }

        $request->user()->guard(['partners'])->sendPhoneVerificationNotification();

        return back()->with('status', 'verification-phone-link-sent');
    }
}
