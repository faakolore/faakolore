<?php

namespace App\Http\Controllers\Auth\Partners;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\EmailVerificationRequest;

class VerifyEmailController extends Controller
{
    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param  \Illuminate\Foundation\Auth\EmailVerificationRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function __invoke(EmailVerificationRequest $request): \Illuminate\Http\RedirectResponse
    {
        if ($request->user()->guard(['partners'])->hasVerifiedPhone()
            && $request->user()->guard(['partners'])->hasVerifiedEmail()) {
            return redirect()->intended(RouteServiceProvider::PARTNER_DASHBOARD.'?verified=1');
        }

        if ($request->user()->guard(['partners'])->markEmailAsVerified()) {
            event(new Verified($request->user()->guard(['partners'])));
        }

        return redirect()->intended(RouteServiceProvider::PARTNER_DASHBOARD.'?verified=1');
    }
}
