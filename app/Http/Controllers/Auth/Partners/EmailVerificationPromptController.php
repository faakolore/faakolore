<?php

namespace App\Http\Controllers\Auth\Partners;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Inertia\Inertia;

class EmailVerificationPromptController extends Controller
{
    /**
     * Display the email verification prompt.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        return $request->user()->guard(['partners'])->hasVerifiedPhone()
        && $request->user()->guard(['partners'])->hasVerifiedEmail()
                    ? redirect()->intended(RouteServiceProvider::PARTNER_DASHBOARD)
                    : Inertia::render('Auth/Partners/VerifyEmail', ['status' => session('status')]);
    }
}
