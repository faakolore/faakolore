<?php

namespace App\Http\Controllers\Auth\Partners;

use Inertia\Inertia;
use App\Models\Partner;
use App\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use Propaganistas\LaravelPhone\PhoneNumber;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Auth/Partners/Register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        Validator::validate(['phone'=> $request['phone']],['phone'=>['required','digits:10','phone:GH']]);
        $request['phone'] = PhoneNumber::make($request['phone'],'GH')->formatE164();

        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:partners',
            'phone' => 'required|phone:GH|unique:partners',
            'password' => 'required|string|confirmed|min:8',
        ]);

        $partner = Partner::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => PhoneNumber::make($request['phone'],'GH')->formatE164(),
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($partner));

        Auth::guard('partners')->login($partner);

        return redirect(RouteServiceProvider::PARTNER_DASHBOARD);
    }
}
