<?php

namespace App\Http\Controllers\Auth\Partners;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;

class EmailVerificationNotificationController extends Controller
{
    /**
     * Send a new email verification notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if ($request->user()->guard(['partners'])->hasVerifiedPhone()
            && $request->user()->guard(['partners'])->hasVerifiedEmail()) {
            return redirect()->intended(RouteServiceProvider::PARTNER_DASHBOARD);
        }

        $request->user()->guard(['partners'])->sendMailVerificationNotification();

        return back()->with('status', 'verification-email-link-sent');
    }
}
