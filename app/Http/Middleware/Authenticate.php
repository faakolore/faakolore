<?php

namespace App\Http\Middleware;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;

class Authenticate extends Middleware
{
    /**
     * @var $guards
     */
    protected $guards;

    /**
     * Handle an unauthenticated user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $guards
     * @return void
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function unauthenticated($request, array $guards)
    {
        $this->guards = $guards;
        throw new AuthenticationException(
            'Unauthenticated.', $guards, $this->redirectTo($request)
        );
    }

    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param Request $request
     * @return string|null
     */
    protected function redirectTo($request): ?string
    {
        if (! $request->expectsJson()) {
            return $this->whoCalled($this->guards);
        }
    }

    /**
     * @param $guards
     * @return string
     */
    protected function whoCalled($guards): string
    {
        foreach ($guards as $guard) {
            switch ($guard) {
                case 'carriers':
                    return route('carriers.login');
                case 'partners':
                    return route('partner.login');
                case 'admins':
                    return route('admin.login');
                default:
                    return route('login');
            }
        }
    }
}
