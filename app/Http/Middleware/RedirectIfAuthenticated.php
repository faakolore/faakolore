<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards)
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            switch ($guard) {
                case 'carriers':
                    if (Auth::guard($guard)->check()) {
                        return redirect(RouteServiceProvider::CARRIER_DASHBOARD);
                    }
                    break;
                case 'partners':
                    if (Auth::guard($guard)->check()) {
                        return redirect(RouteServiceProvider::PARTNER_DASHBOARD);
                    }
                    break;
                case 'admins':
                    if (Auth::guard($guard)->check()) {
                        return redirect(RouteServiceProvider::ADMIN_DASHBOARD);
                    }
                    break;
                default:
                    if (Auth::guard($guard)->check()) {
                        return redirect(RouteServiceProvider::HOME);
//                        return redirect()->route('searching');
                    }
                    break;
            }
        }

        return $next($request);
    }
}
