<?php

namespace App\Http\Middleware;

use App\Utilities\Contracts\MustVerifyPhone;
use Closure;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class EnsureUserIsVerified
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param string|null $redirectToRoute
     * @return \Illuminate\Http\RedirectResponse|mixed|void
     */
    public function handle(Request $request, Closure $next, string $redirectToRoute = null)
    {
        if (! $request->user() ||
            (($request->user() instanceof MustVerifyEmail && ! $request->user()->hasVerifiedEmail())
            || ($request->user() instanceof MustVerifyPhone && ! $request->user()->hasVerifiedPhone())
            )) {
            return $request->expectsJson()
                ? abort(403, 'Your phone or email address is not verified.')
                : Redirect::guest(URL::route($redirectToRoute ?: $this->determineRoute($request)));
        }

        return $next($request);
    }

    /**
     * @param Request $request
     * @return string
     */
    public function determineRoute(Request $request): string
    {
        $route = 'phone.verification.notice';
        if ($request->user()->guard == 'carriers'){
            return 'carriers.'.$route;
        }
        elseif ($request->user()->guard == 'partners'){
            return 'partner.'.$route;
        }
        else{
            return $route;
        }
    }
}
