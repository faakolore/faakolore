<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VerifyCarrierRegistration
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $carrier = Auth::guard('carriers')->user();
        $carrierHasRegistered = $carrier->hasVerifiedRegistration->first();
        $CHR = $carrierHasRegistered;

        if ($CHR->verified == true && $CHR->completed == true
            && $CHR->proof_of_documents ==true && $CHR->lanes == true
            && $CHR->vehicle== true) {

            return $next($request);
        }
        elseif ($CHR->verified == false && $CHR->completed == true
            && $CHR->proof_of_documents ==true && $CHR->lanes == true
            && $CHR->vehicle== true) {

            return redirect()->route('carriers.register.completed');
        }
        elseif ($CHR->verified == false && $CHR->completed == false
            && $CHR->proof_of_documents ==true && $CHR->lanes == true
            && $CHR->vehicle== true) {

            return redirect()->route('carriers.register.completed');
        }
        elseif ($CHR->verified == false && $CHR->completed == false
            && $CHR->proof_of_documents ==false && $CHR->lanes == true
            && $CHR->vehicle== true) {

            return  redirect()->route('carriers.register.proof');
        }
        elseif ($CHR->verified == false && $CHR->completed == false
            && $CHR->proof_of_documents ==false && $CHR->lanes == false
            && $CHR->vehicle== true) {

            return  redirect()->route('carriers.register.lanes');
        }
        elseif ($CHR->verified == false && $CHR->completed == false
            && $CHR->proof_of_documents ==false && $CHR->lanes == false
            && $CHR->vehicle== false) {

            return redirect()->route('carriers.register.vehicle');
        }
    }
}
