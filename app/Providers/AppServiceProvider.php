<?php

namespace App\Providers;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('address', function ($attribute, $value) {
            return !empty($value) && Http::get('https://maps.googleapis.com/maps/api/geocode/json', [
                'address' => str_replace(' ', '+', preg_replace("/[^ \w]+/", "", $value)),
                'key' => config('google.key')
            ])->json()['status'] === 'OK';
        });
    }
}
