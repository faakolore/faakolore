<?php

namespace App\Providers;

use App\Models\Carrier;
use App\Models\Waypoint;
use App\Models\OrderRequest;
use App\Observers\OrdersObserver;
use App\Events\Registered as PCR;
use App\Observers\CarrierObserver;
use App\Observers\WaypointObserver;
use Illuminate\Auth\Events\Verified;
use Illuminate\Auth\Events\Registered;
use App\Listeners\SendMailVerificationNotification;
use App\Listeners\SendPhoneVerificationNotification;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
            SendPhoneVerificationNotification::class,
        ],
        PCR::class => [
          SendMailVerificationNotification::class,
          SendPhoneVerificationNotification::class,
        ],
        Verified::class => [
//            App\Listeners\LogVerifiedUser::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        Carrier::observe(CarrierObserver::class);
        OrderRequest::observe(OrdersObserver::class);
        Waypoint::observe(WaypointObserver::class);
    }
}
