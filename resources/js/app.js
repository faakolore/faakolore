require('./bootstrap');

// Import modules...
import {createApp, h, reactive, ref} from 'vue';
import { App as InertiaApp, plugin as InertiaPlugin } from '@inertiajs/inertia-vue3';
import { InertiaProgress } from '@inertiajs/progress';
import { createStore } from 'vuex';
// import VueTelInput from 'vue3-tel-input'
// import 'vue3-tel-input/dist/vue3-tel-input.css'


const el = document.getElementById('app');

// const VueTelInputOptions = {
//     mode: "national",
//     onlyCountries: [ 'GH'],
//     dropdownOptions: {
//         showDialCodeInSelection: true,
//     },
//     inputOptions: {
//         name:'form.phone',
//         required:true,
//     },
//     validCharactersOnly: true,
//     invalidMsg:'The phone number is invalid',
// }

const store = createStore({
    state: {
            orderForm: {
                pickup: reactive(null),
                // pickup1: ref(null),
                dropoff: reactive(null),
                // dropoff1: ref(null),
                load_type: reactive(null),
                quantity: reactive(null),
                lat: reactive(null),
                lng: reactive(null),
                latD: reactive(null),
                lngD: reactive(null),
            }
    },
    getters:{

    },
    actions:{

    },
    mutations: {
        mergeOrderForm(state, orderForm){
            state.orderForm = orderForm
        }
    }
})

const VueApp = createApp({
    render: () =>
        h(InertiaApp, {
            initialPage: JSON.parse(el.dataset.page),
            resolveComponent: (name) => require(`./Pages/${name}`).default,
        }),
})
    .mixin({ methods: { route } })
    .use(InertiaPlugin);
    // VueApp.use(VueTelInput,VueTelInputOptions);
VueApp.use(store);
    VueApp.mount(el);



InertiaProgress.init({ color: '#38e728' });
